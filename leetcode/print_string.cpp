#include<iostream>
#include<string>
#include<stack>
#include<cctype>
using namespace std;
//输入有如下规则的字符串
//规则： 数字 左中括号 字符串1，右中括号
//输入 2[a1[b]]  输出 abab

void print_string(string& s){
	string res;
	string tmp;
	stack<string>s_str;
	stack<int>s_num;
	int cnt=0;
	for (int i=0;i<s.size();++i){
		if(isdigit(s[i]))
		cnt=cnt*10+s[i]-'0';//如果是数字保存在cnt中
		else if(s[i]=='['){//把当前的cnt压入栈中
			s_num.push(cnt);
			s_str.push(tmp);//把tmp压入栈中
			cnt=0;
			tmp.clear();//清空 保存其余的字符串
		}
		else if(s[i]==']'){//取出数字栈的栈顶元素
			int t=s_num.top();
			s_num.pop();
			for(int j=0;j<t;++j)
				s_str.top()+=tmp;	//把字符串栈顶复制t份
				tmp=s_str.top();//复制到tmp中
				s_str.pop();//把复制后的字符串出栈
		}
		else
		tmp+=s[i];
	}
	if(s_str.empty())
	cout<<tmp<<endl;
}
/*string decode(string s, int& i);
string decodeString(string s) {
	int i = 0;
	return decode(s, i);
}
string decode(string s, int& i) {
	string res = "";
	int n = s.size();
	while (i < n && s[i] != ']') {
		if (s[i] < '0' || s[i] > '9') {
			res += s[i++];
		} else {
			int cnt = 0;
			while (i < n && s[i] >= '0' && s[i] <= '9') {
				cnt = cnt * 10 + s[i++] - '0';
			}
			++i;
			string t = decode(s, i);
			++i;
			while (cnt-- > 0) {
				res += t;
			}
		}
	}
	return res;
}*/
int main(){
	string s ("3[a2[b]]");
	print_string(s);	
}
