#include<iostream>
using namespace std;

int ret(int a){
	return a+6;
}

typedef int(*fun)(int);
typedef int (*fn)(fun);
int re(fun a){
	int n=9;
	return a(n);
}
template <typename T>
class base{
	public:
	T add(const T&a,const T&b){
		return a+b;
	}
};
class derive:public base<int>{
};
struct BS{
	unsigned int color;
	void print(){cout<<"BS"<<endl;}
};
struct car:public BS{
	
};
struct truck:public BS{
};
struct city:public car,public truck {

};
int main(){
	/*int a=0;
	fun f =ret;
	fn g=re;
	auto b=f(a);
	auto c=g(ret);
	cout<<b<<endl;
	cout<<c<<endl;*/
	derive d;
	cout<<d.add(2,3)<<endl;
	city c;
	car r;
	r.print();
	r.color=8;
	c.car::color=3;
	cout<<r.color<<endl;
	cout<<c.car::color<<endl;
}
