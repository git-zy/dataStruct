#include<iostream>

using namespace std;
//把一个有序数组 翻转后找到最小值  无重复元素
//例  01234567  34567012  找到0
int rotate_min(const int* a,int len){
	int low=0;
	int high=len-1;
	int mid;
	while(low<high){
		mid=(low+high)/2;
		if(a[mid]<a[high])
			high=mid;
		else if(a[mid]>a[high])
			low=mid+1;
	}
	return a[low];
}
void test(){
	int a[]={3,4,5,6,7,8,0,1,2};
	int ret=rotate_min(a,sizeof(a)/sizeof(int));
	cout<<"min number is "<<ret<<endl;
}
int main(){
	test();
}
