#include<iostream>
#include<stack>
#include<string>
using namespace std;

int bracket(char *str){//括号匹配 ，成功匹配输出匹配的对数否则输出-1 
		
	stack<char>s;
	string tmp(str);
	if(tmp[0]==')')
	return -1;
	if(tmp.size()%2==1)
	return -1;
	s.push(tmp[0]);
	int num=1;
	int count=0;
	for(int i=1;i<tmp.size();++i){
		if(tmp[i]=='('){
			s.push(tmp[i]);
			++num;
		}
		if(num&&tmp[i]==')'&&s.top()=='('){
			num--;
			++count;
			s.pop();
		}
	}
	if(count<tmp.size()/2)
	return -1;
	return count;
}
