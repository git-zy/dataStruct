#include<iostream>
#include<stack>
#include<string>
/*
给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
有效字符串需满足：
左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
注意空字符串可被认为是有效字符串
 */
using namespace std;
bool check_left(char c);
bool check(char left,char right);
bool isValid(string s) {
	if(s.empty())
		return true;
	if(s.size()%2==1)
		return false;
	int i=0;
	int len=s.size();
	if(!check_left(s[i]))
		return false;
	stack<char>sta;
	sta.push(s[i]);
	for(i=1;i<len;i++){
		if(check_left(s[i]))
			sta.push(s[i]);
		else if(check(sta.top(),s[i])){
			sta.pop();
		}
		else return false;
	}
	return sta.empty(); 
}
bool check(char left,char right){
	if((left=='(')&&right==')')
		return true;
	if((left=='{')&&(right=='}'))
		return true;
	if((left=='[')&&(right==']'))
		return true;
	return false;
}
bool check_left(char c){
	if((c=='[')||(c=='(')||c=='{')
		return true;
	return false;
}
