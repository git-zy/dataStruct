#ifndef STACK_H
#define STACK_H
#include<iostream>

#include<cassert>
template <typename T>
struct Node{//模板实现时要和.h在同一个文件中否则  error LNK2019
	Node* next;
	T value;
};
template <typename T>
class Stack{
public:
	Stack() :current(nullptr){}
	void pop();
	void push(T value);
	bool myempty();
	T top();
	void print();
	Stack operator=(const Stack&) = delete;
	Stack(const Stack&) = delete;
	~Stack();
private:
	Node<T>* current;
};


template <typename T>
Stack<T>::~Stack(){
	while (current){
		Node<T>* pre = current;
		current = current->next;
		delete pre;
	}

}
template <typename T>
void Stack<T>::push(T value){
	Node<T>*s = new Node<T>;
	s->value = value;
	s->next = current;
	current = s;
}
template <typename T>
bool Stack<T>::myempty(){
	return current == nullptr;
}
template <typename T>
void Stack<T>::pop(){
	Node<T>* pre = current;
	assert(!myempty());
	current = current->next;
	delete pre;

}
template <typename T>
T Stack<T>::top(){
	assert(!myempty());
	return current->value;
}
template <typename T>
void Stack<T>::print(){
	Node<T>*p = current;//使用current的副本 
	while (p){
		cout << p->value << " ";
		p = p->next;
	}
}

#endif