#include"stack.h"
using namespace std;
int main(){
	Stack <int> s;
	int a[] = { 1, 2, 3, 4 };
	for (const auto&v : a)
		s.push(v);
	s.print();
	cout << s.top() << endl;
	s.pop();
	s.print();
	cout << s.top() << endl;
	system("pause");
	return 0;
}