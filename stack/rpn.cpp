#include<iostream>
#include<string>
#include<stack>
#include<cstdlib>
using namespace std;
//计算逆波兰表达式的值 有效操作为 +-%/ 每个操作都是整数
/*
	若当前字符是操作数,则压栈
	若当前字符是操作符,则弹出栈中的两个操作数,计算后仍然压入栈中
	若某次操作  栈内无法弹出两个操作数,则表达式有误
*/
bool isOperation(const char* c){
	return ((c[0]=='+')||(c[0]=='-')||(c[0]=='*')||(c[0]=='/'));
}
int rpn (const char* str[],int size){
//	if(str.empty()) return 0;
	stack<int>s;
	int a,b;
	const char* token;
	for(int i=0;i<size;i++){
		token=str[i];
		if(!isOperation(token))
		s.push(atoi(str[i]));
		else{	
			b=s.top();
			s.pop();
			a=s.top();
			s.pop();
			if(token[0]=='+')
			s.push(a+b);
			else if(token[0]=='-')
			s.push(a-b);
			else if(token[0]=='*')
			s.push(a*b);
			else 
			s.push(a/b);
		}
	}
	return s.top();
}
int main(){
	const char *s[]={"2","1","+","3","*"};
	auto ret=rpn(s,sizeof(s)/sizeof(const char*));
	cout<<"result is "<<ret<<endl;
}
