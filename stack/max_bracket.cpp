#include<iostream>
#include<string>
#include<stack>
#include<algorithm>

using namespace std;
/*给定字符串,仅包含左括号和右括号它可能不是括号匹配的设计算法找出最长匹配的
括号子串,返回该子串的长度
例 (()  2
()()4   ()(()) 6*/
/*
记起始匹配位置为start=-1 最大匹配长度为ml=0
考察第i为字符c
如果c为左括号  压栈
如果c为右括号  它一定与栈顶左括号匹配
	如果栈为空,表示没有匹配的左括号,start=i,为下一次可能的匹配做准备
	如果栈不空,出栈 因为和c匹配:
		如果栈为空i-start即为当前找到的匹配长度,检查i-start是否比ml更大,是的ml得以更新
		如果栈不空,则当前栈顶元素t是上次匹配的最后位置,检查i-t是否比ml更大,是的ml得以更新
因为入栈的一定是左括号,可以将他们在字符串中的索引
*/
int max_bracket(string s){
	if(s.empty())return 0;
	int start=-1;
	int ml=0;
	stack<int>sta;
	for(int i=0;i<s.size();++i){
		if(s[i]=='(')
		sta.push(i);
		else{//为右括号
			if(sta.empty())start=i;
			else{
			sta.pop();
			if(sta.empty())
				ml=max(ml,i-start);
			else
				ml=max(ml,i-sta.top());
			}
		}
	}
	return ml;
}
int main(){
	string s("(()())()(");
	auto ret=max_bracket(s);
	cout<<"max match bracket length is "<<ret<<endl;
}
