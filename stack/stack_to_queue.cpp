#include<iostream>
#include<stack>
using namespace std;

class Myqueue{
	private:
	stack<int>s1;
	stack<int>s2;
	public:
	void push(int value);
	void pop();
	bool empty(){return s1.empty();}
	int size(){return s1.size();}
	int top();
};
void Myqueue::push(int value){
	s1.push(value);
}
int Myqueue::top(){
	int temp;
	while(s1.size()!=1){
		temp=s1.top();
		s1.pop();
		s2.push(temp);
	}
	temp=s1.top();
	while(!s2.empty()){
		int t=s2.top();
		s2.pop();
		s1.push(t);
	}
	return temp;
}
void Myqueue::pop(){
	int temp;
	while(s1.size()!=1){
		temp=s1.top();
		s1.pop();
		s2.push(temp);
	}
	s1.pop();
	while(!s2.empty()){
		temp=s2.top();
		s2.pop();
		s1.push(temp);
	}
}
void test(){
	Myqueue s;
	for(int i=0;i<4;i++)
	s.push(i);
	cout<<"s.top="<<s.top()<<endl;
	cout<<"s.size"<<s.size()<<endl;
	s.pop();
	cout<<"s.top="<<s.top()<<endl;
	s.pop();
	s.pop();
	s.push(22);
	cout<<"s.top="<<s.top()<<endl;
	cout<<"s.size"<<s.size()<<endl;
	s.pop();
	cout<<"s.top="<<s.top()<<endl;
	cout<<"s.empty()="<<s.empty()<<" s.size()="<<s.size()<<endl;
}
int main(){
	test();
}

