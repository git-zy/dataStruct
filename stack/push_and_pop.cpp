#include<iostream>
#include<stack>

using namespace std;

/*
	给定无重复元素的两个等长数组,分别表述入栈序列和出栈序列  问这样的出栈序列可行吗?
	如  入栈 ABCDEFG  出栈 BAEDFGC  可行  
	入栈ABCD  出栈BDAC 不可行
	使用一个堆栈S模拟压栈 出栈操作 入栈序列为A 出栈序列为B
	遍历B的每个元素b
	情形1：若b等于栈顶元素S,恰好匹配，则检查B的下一个元素，栈顶元素出栈
	情形2：若b不等于栈顶元素s，则将A的当前元素入栈，目的是希望在A的剩余元素中找到b
	在情形1中，若栈S为空，则认为b无法与栈内元素匹配，调用情形2
*/
bool IsPossible(const char*in,const char*out){
	stack<char>s;
	while(*out){
		if(!s.empty()&&(s.top()==*out)){
				s.pop();
				out++;
			}
			else{
				if(*in=='\0')
				return false;
				s.push(*in);
				in++;
			}
	
	}
	return true;
}
int main(){
	const char in[]="ABCD";
	const char out[]="BDAC";
	bool ret=IsPossible(in,out);
	cout<<"result is "<<(ret?"true":"false")<<endl;
}
