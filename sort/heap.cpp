#include"heap.h"
#include<algorithm>
bool HEAP::Insert(int value){//插入新的节点
	size++;
	a[size]=value;
	Adjust(size);
	return true;
}
bool HEAP::Adjust(int pos ){//插入的节点总是在最后一个

	while(pos>1){//循环和他的父节点比较
		int parent=pos/2;//父节点的索引号
		if(a[pos]<a[parent]){//<  升序  >  降序
		swap(a[pos],a[parent]);//交换父子节点的值	
		pos=parent;//当前结点位置上移
		}
		else
		break;
	}
	return true;
}
void HEAP::print(){
	for(int i=1;i<=size;i++)
	cout<<a[i]<<" ";
	cout<<endl;
}
int HEAP::delete_root(){//向下删除
	int Max=a[1];
	int pos=1;
	int left=pos*2;//左孩子结点
	while(left<=size)	{
		int temp=a[left];
		//>升序 <降序
		if(left+1<=size&&temp>a[left+1]){//右孩子的编号小于总size 且左孩子小于右孩子
		++left;
		temp=a[left];//右孩子结点的值
		}
		a[pos]=temp;
		pos=left;
		left*=2;//一直往下寻找
	}
	a[pos]=a[size];//最后一个节点的值赋值给当前结点
	--size;
	return Max;
}
