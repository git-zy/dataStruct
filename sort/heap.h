#ifndef HEAP_H
#define HEAP_H
#include<iostream>

using namespace std;

typedef class HEAP{
public:
	HEAP(int n):a(new int[n]()),size(0){}
	~HEAP(){
			if(a){
		delete []a;	
		a=nullptr;
		}
	}
	bool Insert(int value);//插入新的节点
	bool Adjust(int pos);//比较调整新插入的位置
	void print();
	int delete_root();//删除根并返回根的关键字
private:
	int*a;//指向数据的指针
	int size;//当前堆中的元素个数
}Heap;





#endif
