#ifndef SORT_H
#define SORT_H
#include<iostream>
#include<algorithm>
#include<utility>
void Print(int *a,int n){
	for (int i = 0; i != n; ++i){
		std::cout << a[i] << " ";
		if ((i + 1) % 5 == 0)
			std::cout << "\n";
	}
		
}
void BubbleSort1(int *a, int n){
	int i, j, temp, count1 = 0, count2 = 0;
	for (i = 0; i < n - 1; i++){
		for (j = i + 1; j < n; j++){
			count1++;
			if (a[j]<a[i]){
				count2++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;	
			}
		}
	}
	std::cout << "��� " << count1 << " ��� " << count2 << std::endl;
}
void BubbleSort2(int *a, int n){
	int i, j, temp,count1=0,count2=0;
	bool flag = true;
	for (i = 0; i < n - 1 && flag; i++){
		flag = false;
		for (j = n-1; j >i ; j--){
			count1++;
			if (a[j]<a[j-1]){
				count2++;
				temp = a[j-1];
				a[j-1] = a[j];
				a[j] = temp;
				flag = true;
			}
		}
	}
	std::cout << "�Ƚ��� " << count1 << " ������ " << count2 << std::endl;
}
void InsertSort(int *a, int n){
	int i, j,temp;
	for (i = 1; i != n; ++i){
		temp = a[i];//���������
		for (j = i - 1; j >=0&&temp<a[j]; --j){			//��������,�����������������
				a[j + 1] = a[j];
		}
		a[j+1] = temp;//���������������������
	}
}
void SelectSort(int *a, int n){
	int i, j,min,temp;
	for (i = 0; i != n-1; ++i){
		min = i;//���������
		for (j = i + 1; j != n; ++j){
			if (a[j] < a[min])
				min = j;
		}
		if (min != i){
			temp = a[i];
			a[i] = a[min];
			a[min] = temp;
		}
	}
}
void HellSort(int a[], int n)
{
	int i, j, temp;
	int gap = n;
	do{
		gap = gap / 3 + 1;
		for (i = gap; i<n; i++)
		{
			temp = a[i];//�������  
			for (j = i - gap; j >= 0 && temp<a[j]; j -= gap)//����� 
			{
				a[j + gap] = a[j];
			}
			a[j + gap] = temp;
		}
	} while (gap>1);
}

void merging(int *a1,int size1,int*a2,int size2){
	int *temp=new int[size1+size2];
	int i=0,j=0,k=0;
	while(i<size1&&j<size2){
		if(a1[i]<a2[j])
		temp[k++]=a1[i++];
		else
		temp[k++]=a2[j++];
		}
	while(i<size1)
	temp[k++]=a1[i++];
	while(j<size2)
	temp[k++]=a2[j++];
	for(int i=0;i<(size1+size2);++i)
	a1[i]=temp[i];
	delete temp;
}
void merge_sort(int *a,int size){
	if(size>1){
		int *list1=a;
		int list1_size=size/2;
		int *list2=a+size/2;
		int list2_size=size-list1_size;
		
		merge_sort(list1,list1_size);
		merge_sort(list2,list2_size);
		
		merging(list1,list1_size,list2,list2_size);
	}
}
void quick_sort_left(int *a,int left,int right){
	if(left<right){
	int i=left;
	int j=right;
	int x=a[i];
	while(i<j){
		while(i<j&&x<a[j])//����������x��
		--j;
		if(i<j)
		a[i++]=a[j];
		while(i<j&&x>a[i])//����������x��
		++i;
		if(i<j)
		a[j--]=a[i];
	}
	a[i]=x;
	quick_sort_left(a,left,i-1);
	quick_sort_left(a,i+1,right);
	}
}
void quick_sort_right(int *a,int left,int right){
	if(left<right){
		int i=left;
		int j=right;
		int x=a[j];
		while(i<j){
			while(i<j&&x<a[i])//�����������x��
			++i;
			if(i<j)
			a[j--]=a[i];
			while(i<j&&x>a[j])//�����������x��
			--j;
			if(i<j)
			a[i++]=a[j];
		}
		a[j]=x;
		quick_sort_right(a,left,j-1);
		quick_sort_right(a,j+1,right);

	}
}
void adjust_heap(int *a,int n,int pos){
	int temp=a[pos];
	int i;
	for(i=2*pos;i<n;i*=2){
		if(a[i]<a[i+1])
		i++;
		if(temp>=a[i])
		break;
		a[pos]=a[i];
		pos=i;
	}
	a[pos]=temp;	

}
void heap_sort(int*a ,int size){
	int i;
	int temp;
	for(i=size/2;i>0;i--)
	adjust_heap(a,size,i);
	for(i=size;i>0;i--){
		temp=a[i];
		a[i]=a[1];
		a[1]=temp;
		adjust_heap(a,i-1,1);
	}
	
}
#endif
