#include"link.h"
#include<cassert>
int main(){
	
	LinkList<int> lst;
	cout << lst.LinkEmpty() << endl;
	for (int i = 0; i < 5;i++)
	lst.LinkTailInsert(rand()%10);
	lst.LinkHeadInsert(0);
	cout << " the  mid value of linklist is " << lst.FindMid() << endl;
	cout << lst.LinkEmpty() << endl;
	lst.Print();
	lst.Reverse();
	lst.Print();
	int a = 0;
	cout << "LinkList size is " << lst.LinkSize() << endl;
	lst.LinkDelete(a);
	cout << "LinkList size is " << lst.LinkSize() << endl;
	lst.Print();
	cout << " the  mid value of linklist is " << lst.FindMid() << endl;
	auto n = lst.GetNode();
	while (n != nullptr){
		cout << n->value<<" ";
		n = n->next;
	}
	std::cout << "\n";
	lst.Sort();
	lst.Print();
	auto key = lst.Find_k_Node(5);
	assert(key);
	cout << key->value << endl;
	system("pause");
	return 0;
	
}