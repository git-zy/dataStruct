#include<iostream>
#include<algorithm>
#include<cassert>
using namespace std;
typedef struct NODE{
	int value;
	NODE *next;
	NODE(int v=0):value(v),next(nullptr){}
}Node;
void print(Node* head){
	auto cur=head->next;
	while(cur->next){
		cout<<cur->value<<"->";
		cur=cur->next;
	}
	cout<<cur->value;
	cout<<endl;
}
void Destroy(Node *head){
	Node* cur;
	while(head){
		cur=head->next;
		delete head;
		head=cur;
	}
}
Node* add(Node* head1,Node*head2){
	Node* Sum=new Node(0);
	Node* tail=Sum;
	Node*cur;
	auto p1=head1->next;
	auto p2=head2->next;
	int carry=0;
	int value;
	while(p1&&p2){
		value=p1->value+p2->value+carry;
		carry=value/10;
		value%=10;
		cur=new Node(value);
		tail->next=cur;
		tail=cur;
		p1=p1->next;
		p2=p2->next;
	}
	auto p=p1?p1:p2;//处理较长的链表
	while(p){
	value=p->value+carry;
	carry=value/10;
	value%=10;
	cur=new Node(value);
	tail->next=cur;
	tail=cur;
	p=p->next;
	}
	if(carry)
	tail->next=new Node(carry);
	return Sum;
}
void sort_link(Node* head){
	if(head==nullptr&&head->next==nullptr)	
	return;
	Node*p=nullptr;
	bool isChange=true;
	while(p!=head->next&&isChange){
		auto q=head;
		isChange=false;
		for(;q->next&&q->next!=p;q=q->next){
			if(q->value>q->next->value){
				swap(q->value,q->next->value);
				isChange=true;
			}
		}
		p=q;
	}
}
void partial_reverse(Node* head,int begin,int end){
	assert(begin>0&&end>0);
	int count=0;
	auto tmp=head->next;
	while(tmp){
	++count;
	tmp=tmp->next;
	}
	if(end>count)
	end=count;
	int i;
	auto cur=head->next;
	for(i=0;i<begin-1;++i){
		head=cur;
		cur=cur->next;
	}
	auto pre=cur;
	cur=cur->next;
	Node* node;
	for(;i<end-1;++i){//画个图看下
		node=cur->next;
		cur->next=head->next;
		head->next=cur;
		pre->next=node;
		cur=node;
		
	}
	
}
 void deleteDump(Node *head){//删除重复节点 保留最开始的重复节点  7 7 7 保留第一个7
	Node *pre=head->next;
	Node*cur;
	while(pre){
		cur=pre->next;
		if(cur&&cur->value==pre->value){//下移个节点  非空再解引用
			pre->next=cur->next;
			delete cur;
		}
		else
		pre=cur;
	}
}
void deleteDump2(Node*head){//删除重复节点 保留最后的重复节点 
	Node*pre=head;
	Node*cur=pre->next;
	Node* node;
	while(cur){
		while((node=cur->next)&&cur->value==node->value){
			pre->next=node;
			delete cur;
			cur=node;
		}
		pre=cur;
		cur=node;
	}
}
void deleteDump3(Node*head){//删除所有的重复节点  不保留
	
	Node* pre=head;
	Node* cur=pre->next;
	Node* node;
	bool dup;
	while(cur){
		dup=false;
	while((node=cur->next)&&cur->value==node->value){
		
		pre->next=node;
		delete cur;
		cur=node;
		dup=true;
	}
	if(dup){//此时cur和源数据重复  删除  node 指向的是下一个节点
		pre->next=node;
		delete cur;
	}	
	else
	pre=cur;
	
	cur=node;
	}
}
//给定一个链表和一个值x 将链表划分为两部分划分后小于X的节点在前，大于X的节点在后
void partion(Node*head,int x){
	Node*	left_head=new Node(0);
	Node* right_head=new Node(0);
	auto left=left_head;
	auto right=right_head;
	auto cur=head->next;
	while(cur){
		if(cur->value<x){
			left->next=cur;
			left=cur;
		}
		else{
			right->next=cur;
			right=cur;
		}
		cur=cur->next;
	}
	//将right链接在left尾部
	left->next=right_head->next;
	right->next=nullptr;
	head->next=left_head->next;
	delete right_head;
	delete left_head;
}
int get_length(Node*head)	{
	int len=0;
	while(head){
	++len;
	head=head->next;
	}
	return len;
}
Node* find_common(Node*head1,Node*head2){
	int m=0,n=0;
	auto p1=head1->next;
	auto p2=head2->next;
	m=get_length(p1);
	n=get_length(p2);
	if(n>m){
		swap(p1,p2);
		swap(m,n);
	}
	for(int i=0;i<m-n;++i){//空转m-n次保证两个链表以相同长度开始比较  前m-n个节点中不会存在公共节点
		p1=p1->next;
	}
	while(p1){
		if(p1->value==p2->value)
		return p1;
		p1=p1->next;
		p2=p2->next;
	}
	return nullptr;
}
int main(){
	
	Node*head1=new Node(0);
	for(int i=0;i<6;++i){
		Node*p=new Node(rand()%10);
		p->next=head1->next;
		head1->next=p;
	}
	Node*head2=new Node(0);
	for(int i=0;i<9;++i){
		Node*p=new Node(rand()%10);
		p->next=head2->next;
		head2->next=p;
	}
	cout<<"head1...\n";
	print(head1);
	cout<<"head2...\n";
	print(head2);
	
	auto common_node=find_common(head2,head1);
	if(common_node)
	cout<<"first common node value is "<<common_node->value<<endl;
	auto sum=add(head1,head2);
	cout<<"after add...\n";
	print(sum);
	partial_reverse(sum,1,92);
	cout<<"after paritial_reverse...\n";
	print(sum)	;
	
	partion(sum,5);
	cout<<"after parition...\n";
	print(sum);

	sort_link(sum);
	cout<<"after sort_link...\n";
	print(sum);
	deleteDump3(sum);
	cout<<"after delete elem...\n";
	print(sum);
		
	
	Destroy(head1);
	Destroy(head2);
	Destroy(sum);
}
