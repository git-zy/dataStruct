#ifndef LINK_H
#define LINK_H
#include<iostream>
using namespace std;
template <typename T>
struct Node{
	Node() :next(nullptr){}
	struct Node<T>* next;
	T value;
};
template <typename T>
class LinkList{
public:
	LinkList() :node(nullptr),num(0){
		/*node = new Node<T>;
		node->next = nullptr;*/
	}
	~LinkList();
	void LinkHeadInsert(const T &data);
	void LinkTailInsert(const T& data);
	void LinkDelete(T& data);
	bool LinkEmpty();
	int  LinkSize();
	void Print();
	int FindMid();
	void Reverse();
	void Sort();
	Node<T>*Find_k_Node(const T&value);//寻找倒数第value个节点
	Node<T>*GetNode();
private:
	
	struct Node<T>* node;
	int num;
};

template <typename T>
LinkList<T>::~LinkList(){
	num = 0;
	while (node){
		auto pre = node;
		node = node->next;
		delete pre;
	}
	cout << "destructor\n";
}
template <typename T>
void LinkList<T>::LinkHeadInsert(const T& data){
	Node<T> *temp = new Node<T>;
	temp->value = data;
	temp->next = node;
	node = temp;
	++num;
}
template <typename T>
void LinkList<T>::LinkTailInsert(const T& data){
	Node<T> *temp = new Node<T>;
	temp->value = data;
	
	if (node == nullptr){
		temp->next = nullptr;
		node = temp;
	}
	else{
		temp->next = node->next;
		node->next = temp;
	}
	
	++num;
}
template <typename T>
void LinkList<T>::LinkDelete( T& data){
	Node<T>*pre;
	pre = node;
	/*if (node->value == data){
		node = node->next;
		delete pre;
		pre = nullptr;	
		--num;
	}*/
	
	while ((pre != nullptr) && (pre->next->value != data))
		pre = pre->next;
	if (pre == nullptr)
		return;
	else {
		auto p = pre->next;
		pre->next = p->next;
		delete p;
	}
	--num;
}

template <typename T>
bool LinkList<T>::LinkEmpty(){
	return num == 0;
}

template <typename T>
void LinkList<T>::Print(){
	auto pre= node;
	while (pre){
		cout <<pre->value<< " ";
		pre = pre->next;
	}
	cout << "\n";

}
template <typename T>
void LinkList<T>::Reverse(){
	Node<T>*next=nullptr;
	Node<T>*pre=nullptr;
	while (node){
		next = node->next;
		node->next = pre;
		pre = node;
		node = next;
	}
	node = pre;
	
}
template <typename T>
int LinkList<T>::LinkSize(){
	return num;
}

template <typename T>
int LinkList<T>::FindMid(){
	auto fast = node;
	auto slow = node;
	while (fast->next != nullptr){
		/*fast = fast->next->next;
		slow = slow->next;
		if (fast == nullptr)
			break;*/
		if (fast->next->next != nullptr){
			fast = fast->next->next;
			slow = slow->next;
		}
		else
			fast = fast->next;//结束了
	}
	return slow->value;
}
template <typename T>
 Node<T>*LinkList<T>::GetNode(){
	return node;
}
 template <typename T>
 void LinkList<T>::Sort(){
	 bool isChange = true;
	 auto pre = node;
	 while (pre&&isChange){
		 auto cur = pre;
		 isChange = false;
		 for (; cur; cur = cur->next){
			 if (cur->value <pre->value){
				 auto temp = cur->value;
				 cur->value = pre->value;
				 pre->value = temp;
				 isChange = true;
			 }
		 }
		 pre = pre->next;
	 }
 }
 template <typename T>
 Node<T>* LinkList<T>::Find_k_Node(const T&value){
	 auto pre = node;
	 auto cur = node;
	 if (value<=0 || value>num)
		 return nullptr;
	 for (int i = 0; i < value - 1; ++i)
		 pre = pre->next;
	 while (pre->next){
		 pre = pre->next;
		 cur = cur->next;
	 }
	 return cur;
 }

#endif 