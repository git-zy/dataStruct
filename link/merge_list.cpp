#include <iostream>

using namespace std;

typedef struct NODE{
	NODE *next;
	int value;
	NODE(int val=0):value(val),next(nullptr){}
}Node;

Node* merge_list(Node* head1,Node*head2){
	
	if(head1==nullptr||head2==nullptr){
			if(head1==nullptr&&head2==nullptr)
			return nullptr;
			else
			return head1==nullptr?head2:head1;
	}

	Node* node;
	if(head1->value>head2->value){
		node=head2;
		node->next=merge_list(head1,head2->next);
	}
	else{
		node=head1;
		node->next=merge_list(head1->next,head2);
	}
	
	return node;
}
Node* merge(Node*head1,Node*head2){
	if(head1==nullptr) return head2;
	if(head2==nullptr) return head1;
	Node* node=new Node(0);
	auto head=node;
	auto p1=head1->next;
	auto p2=head2->next;
	while(p1&&p2){	
		if(p1->value<p2->value){
		Node*p =new Node(p1->value);
		head->next=p;
		head=p;
		p1=p1->next;
		}
		else{
		Node *p=new Node(p2->value);
		head->next=p;
		head=p;
		p2=p2->next;
		}
	}
	auto p=p1?p1:p2;
	head->next=p;
	return node;
}
void print(Node*head){
	auto cur=head->next;
	while(cur->next){
		cout<<cur->value<<"->";
		cur=cur->next;
	}
	cout<<cur->value;
	cout<<endl;
}
int main(){
	
	Node*head1=new Node(0);
	for(int i=6;i>0;--i){
		Node* cur=new Node(i);
		cur->next=head1->next;
		head1->next=cur;
	}
	Node*head2=new Node(0);
	for(int i=10;i>4;--i){
		Node* cur=new Node(i);
		cur->next=head2->next;
		head2->next=cur;
	}
	print(head1);
	print(head2);
	//Node* node=new Node(0);//带头结点的链表
  //node->next=merge_list(head1->next,head2->next);
  auto node=merge(head1,head2);
	print(node);
}