#include<iostream>
#include"binary_tree.h"

using namespace std;
/*
若某个节点的左右子树都是二叉搜索树,且能计算出该节点左子树最大值max和右子树
的最小值min,记该节点的值为value
若value>max且value<min
则该节点形成了更大的二叉搜索树 
*/
void print(int value){
	cout<<value<<" ";
}
void change_value(int value){
	value=rand()%100;
}
int main(){
	//构造输入数据 
	BinaryTree t;
	for(int i=0;i<10;i++)
		t.Insert(rand()%100);
	t.InOrder(change_value);

	t.InOrder(print);//中序
	cout<<endl;
	t.PreOrder(print);//前序
	cout<<endl;
	//计算
	TreeNode * node;
	int max_number=t.LargestBST(node);
	cout<<node->value<<'\t'<<max_number<<endl;
	cout<<"after reverse\n";
	t.Reverse();	
	t.InOrder(print);//中序
	cout<<endl;
	t.PreOrder(print);//前序
	cout<<endl;
}
