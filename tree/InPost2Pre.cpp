#include<iostream>
using namespace std;
/*
	给定后序和中序  求出前序遍历
	后 AEFDHZMG
	中 ADEFGHMZ
*/
void InPre2Post(const char* in,const char*post,int len,char* pre,int& index) {
	if(len<=0)
	return ;
	if(len==1){
		pre[index]=*post;
		index++;
		return ;
	}
	char root=post[len-1];//根节点
	int nRoot=0;
	for(;nRoot<len;nRoot++)
	if(in[nRoot]==root)//找到根节点在中序中的位置
	break;
	pre[index]=root;//前序 先放根的位置
	index++;
	InPre2Post(in,post,nRoot,pre,index);//index需要改变 传引用 根节点的前半段
	InPre2Post(in+nRoot+1,post+nRoot,len-nRoot-1,pre,index);//根节点的后半段  in 多加1 多了个根  post不能加1
}
int main(){
	char post[]="AEFDHZMG";
	char in[]="ADEFGHMZ";
	int size=sizeof(in)/sizeof(char);
	char* pre=new char [size];
	int index=0;
	InPre2Post(in,post,size-1,pre,index);
	pre[size-1]='\0';
	cout<<pre<<endl;
	delete []pre;
	return 0;
}

