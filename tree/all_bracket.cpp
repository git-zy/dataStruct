#include<iostream>
#include<vector>
#include<string>
using namespace std;
/*
n对括号能够得到的有效括号序列有哪些
n=3时,有效括号串为5个分别为
()()()  ()(())  (())() (()())  ((())) 
任何一个括号序列,可以写成形式A(B)
A B 都是若干括号对组成的合法串(可为空串)
若 n=0 括号序列为空
n=1 括号序列只能是()
算法描述： i属于[0,n-1]
计算i对括号的可行序列A;
计算n-i-1对括号的可行序列B;
组合得到A(B);
加上额外一对括号(),总括号为n对

都可以转为二叉树,本质上是一个catalan数组
*/
void Unit(vector <string>& result,const vector<string>&prefix,
const vector<string>&suffix){
	vector<string>::const_iterator ip,is;
	for(ip=prefix.begin();ip!=prefix.end();ip++){
		for(is=suffix.begin();is!=suffix.end();is++){
			result.push_back("");
			string&r=result.back();
			r+="(";
			r+=*ip;
			r+=")";
			r+=*is;
		}
	}
}

vector<string> all_bracket(int n){
	if(n==0)
	return vector<string>(1,"");
	if(n==1)
	return vector<string>(1,"()");
	vector<string> prefix,suffix,result;
	for(int i=0;i<n;i++){
		prefix=all_bracket(i);
		suffix=all_bracket(n-i-1);
		Unit(result,prefix,suffix);
	}
	return result;
}
int main(){
	int n;
	cout<<"enter number: ";
	cin>>n;
	cout<<endl;
	auto vec=all_bracket(n);
	int i=0;
	for(const auto& v:vec)
	cout<<i++<<": "<<v<<endl;
}
