#include<iostream>
#include<map>
using namespace std;
/*
	给定字符串str,计算最多包含k个不同字符的最长子串
	eceba   k=3
	包含3个不同字符的最长子串为eceb
	使用双索引i,j初值赋值为0,考察子串str[i,j]
		记子串str[i,j]中包含的不同字符的数目小于等于k为A
		若子串str[i,j]满足A,则j++
		否则i++继续考察
*/

int longest_sub_strk(const char*str,int size,int k,int&from,int&to){
	if(size<=0)
	return 0;
	map<char,int>m;
	int left=0;
	int mx=0;
	for(int right=0;right<size;right++){
		m[str[right]]++;//字典映射 字母与数字
		while(static_cast<int>(m.size())>k){
//新添加进来字母，如果字符串的第一个字母是有重复的-1继续前进直到知道到只出现一次的
//删除后字典内含有的数量小于k right继续向前前进
			m[str[left]]--;
			if(m[str[left]]==0)
			m.erase(str[left]);
			left++;
		}
		if(mx<right-left+1){
			mx=right-left+1;
			from=left;
			to=right;
		}
	}
	return mx;
}
void print (char *s,int from,int to){
	while(from<=to){
		cout<<s[from];
		from++;
	}
	cout<<endl;
}
int main(){
	char s[]="cebea";
	int from,to;
	cout<<longest_sub_strk(s,sizeof(s)/sizeof(char)-1,3,from,to)<<endl;
	cout<<from<<" "<<to<<endl;
	print(s,from,to);
}
