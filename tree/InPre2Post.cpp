#include<iostream>
using namespace std;
/*
	给定前序和中序  求出后序遍历
	前 GDAFEMHZ
	中 ADEFGHMZ
*/
void InPre2Post(const char* in,const char*pre,int len,char* post,int& index) {
	if(len<=0)
	return ;
	if(len==1){
		post[index]=*pre;
		index++;
		return ;
	}
	char root=*pre;//根节点
	int nRoot=0;
	for(;nRoot<len;nRoot++)
	if(in[nRoot]==root)//找到根节点在中序中的位置
	break;
	InPre2Post(in,pre+1,nRoot,post,index);//index需要改变 传引用 根节点的前半段
	InPre2Post(in+nRoot+1,pre+nRoot+1,len-nRoot-1,post,index);//根节点的后半段 都加1 多了个根节点
	post[index]=root;//后序 最后放根的位置
	index++;
}
int main(){
	char pre[]="GDAFEMHZ";
	char in[]="ADEFGHMZ";
	int size=sizeof(in)/sizeof(char);
	char* post=new char [size];
	int index=0;
	InPre2Post(in,pre,size-1,post,index);
	post[size-1]='\0';
	cout<<post<<endl;
	cout<<pre<<endl;
	delete []post;
	return 0;
}
