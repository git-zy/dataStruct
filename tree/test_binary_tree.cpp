#include"binary_tree.h"
void Print(int value){
	std::cout<<value<<" ";
}
int main(){
	BinaryTree t;
	t.Insert(7);
	t.Insert(5);
	t.Insert(3);
	t.Insert(6);
	t.Insert(8);
	t.Insert(10);
	t.Insert(4);
	t.PreOrder(Print);
	std::cout<<"\n";
	t.InOrder(Print);
	std::cout<<"\n";
	t.PostOrder(Print);
	std::cout<<"\n";
	t.Bfs(Print);
//	auto node=t.Find(4);
//	if(node)
//	std::cout<<node->value<<std::endl;
	t.Delete(5);
	std::cout<<"\n";
	t.PreOrder(Print);
	std::cout<<"\n";
	t.InOrder(Print);
	std::cout<<"\n";
	t.PostOrder(Print);
	std::cout<<"\n";
	t.Bfs(Print);
	return 0;
}
