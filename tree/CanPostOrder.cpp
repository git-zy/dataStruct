#include<iostream>
using namespace std;
//给定一个数组 的乱序序列 
//判断这个序列可不可以是二叉查找树的后序遍历  中序遍历是升序的
// 1 2 5 4 3     3 5 1 4 2
bool CanPostOrder(const int *a ,int size){
	if(size<=1)
	return true;
	int root=a[size-1];
	int left=0;
	while(left<size-1){
		if(a[left]>root)
		break;
		left++;
	}
	int right=size-2; //size-1  是根节点
	while(right>=0){
		if(a[right]<root)
		break;
		right--;
	}
	if(right!=left-1) //无法根据root分成两部分
	return false;
	return CanPostOrder(a,left) //左子树
	&&CanPostOrder(a+left,size-left-1);//右子树
}
int main(){
	//int a[]={1,2,5,4,3};
	int a[]={3,5,1,4,2};
	bool b=CanPostOrder(a,sizeof(a)/sizeof(int));
	cout<<b<<endl;
}
