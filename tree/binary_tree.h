#ifndef BINARY_TREE_H
#define BINARY_TREE_H
#include<iostream>
#include<functional>
typedef struct TREENODE{
	int value;
	TREENODE *left;
	TREENODE *right;
	TREENODE(int val):value(val),left(nullptr),right(nullptr){}
}TreeNode;
using VISIT=std::function<void(int)>;
class BinaryTree{
	public:
	BinaryTree();
	~BinaryTree();
	bool Insert(int value);
	bool Delete(int value);
	TreeNode* Find(const int value)const;
	void PreOrder(VISIT visit)const;//前中后序都属于深度优先遍历
	void InOrder(VISIT visit)const ;
	void PostOrder(VISIT visit)const;
	void Bfs(VISIT visit)const;//广度优先 对应层序遍历
	int LargestBST(TreeNode*& node)const;//寻找最大二叉搜索子树
	void Reverse();
	private:
	TreeNode *m_root;
	bool _insert(TreeNode*&root,int value);
	bool _insert2(int value);
	void _preorder(TreeNode* node,VISIT visit)const;
	void _preorder2(VISIT visit)const;
	void _inorder(TreeNode* node,VISIT visit)const;
	void _inorder2(VISIT visit)const;
	void _inorder3(VISIT visit)const;
	void _postorder(TreeNode* node,VISIT visit)const;
	void _postorder2(VISIT visit)const;
	bool _LargestBST(TreeNode* root,int&Min,int& Max,int& count,int& number,TreeNode*& node)const;	
	void _Reverse(TreeNode* node);
	void DeleteChildless(TreeNode* parent,TreeNode*node);//删除叶子节点
	void DeleteSingleSon(TreeNode* parent,TreeNode*node);//删除单支
};
#endif
