#include"binary_tree.h"
#include<stack>
#include<utility>  //pair
#include<queue>
#include<algorithm>
#include<limits.h>
//二叉查找树的中序遍历是升序的 
using namespace std;

BinaryTree::BinaryTree():m_root(nullptr){}
BinaryTree::~BinaryTree(){
	while(!m_root){
		Delete(m_root->value);
	}
	cout<<"all node has been deleted\n";
}

bool BinaryTree::Insert(int value){
	//return _insert(m_root,value);
	return _insert2(value);
}
bool BinaryTree::_insert(TreeNode*& root,int value){//传引用为了修改值
	if(root==nullptr){
	root=new TreeNode(value);
	return true;
	}
	 if(root->value>value)
	return _insert(root->left,value);
	 if(root->value<value)
	return _insert(root->right,value);
	return false;
}
bool BinaryTree::_insert2(int value){//非递归
	if(m_root==nullptr)	{
		m_root=new TreeNode(value);
		return true;
	}
	auto pre=m_root;
	auto cur=m_root;
	while(cur){
		pre=cur;//保存上一个节点位置
		if(value<cur->value)
		cur=cur->left;
		else if(value>cur->value)
		cur=cur->right;
		else
		return false; //等于直接返回
	}
	if(pre->value>value){
		pre->left=new TreeNode(value);
		return true;
	}
	else if(pre->value<value){
		pre->right=new TreeNode(value);
		return true;
	}
	
}	
void BinaryTree::DeleteChildless(TreeNode* parent,TreeNode*node){
	if(node==m_root)
	m_root=nullptr;
	else if(parent->left==node)
	parent->left=nullptr;
	else 
	parent->right=nullptr;
	delete node;
}
void BinaryTree::DeleteSingleSon(TreeNode*parent,TreeNode* node){
	auto grandson=node->left?node->left:node->right;//把要删节点的左子树或者右子树找出
	if(node==m_root)
	m_root=grandson;
	else if(parent->left==node)
	parent->left=grandson;
	else 
	parent->right=grandson;
	delete node;
}

//删除分三种情况
//1 p为叶子节点  直接删除
//2 p为单支节点  它是它的父亲节点的左孩子则用他父亲的左孩子指向被删节点的孩子 右孩子同之
//3 p的左右子树均不空 
//3可以使用两种方法进行删除  
/* 1.使用p的直接后继d(p的右孩子的最左子孙) d一定没有左子树，所以使用单支删除的方式删除d，
	并让d的父亲节点dp成为d的右子树的父亲节点，同时d的值代替p的值
	2.找到p的直接前驱x(p的左孩子的最右子孙)，x一定没有右子树，所以可以删除x并让x的父亲节点
成为x的左子树的父亲节点
*/
bool BinaryTree::Delete(int value){
	if(!m_root)
	return false;
	auto cur=m_root;
	TreeNode* parent=nullptr;
	while(cur){//寻找待删除节点
		if(cur->value<value){
			parent=cur;//parent保存待删除节点的父节点
			cur=cur->right;
		}
		else if(cur->value>value){
			parent=cur;
			cur=cur->left;
		}
		else break;//找到待删除节点
	}
	if(!cur)
	return false;//如果是当前指针为空跳出的循环，则没有找到
	if(!cur->left&&!cur->right)//叶子节点
	DeleteChildless(parent,cur);
	else if(!cur->left||!cur->right)
	DeleteSingleSon(parent,cur);
	else{ //左右孩子都在
		auto temp=cur;//暂时保存待删除节点
		parent=cur;
		cur=cur->left;//寻找直接前驱
		while(cur->right){
			parent=cur;
			cur=cur->right;
		}
		temp->value=cur->value;//使用直接前驱的值代替待删除节点的值，再判断它有没有左孩子
		if(cur->left)
		DeleteSingleSon(parent,cur);//存在左子树，属于单支情况
		else 
		DeleteChildless(parent,cur);
		/*		
		auto temp=cur;//寻找直接后继,右子树的最左子孙，则它一定没有左子树
		parent=cur;
		cur=cur->right;
		while(cur->left){		
			parent=cur;
			cur=cur->right;
		}
		temp->value=cur->value;
		if(cur->right)//判断是否有右子树
		DeleteSingleSon(parent,cur);
		else
		DeleteChildless(parent,cur);
		*/
	}
	return true;
}
TreeNode* BinaryTree::Find(const int value)const{
	if(m_root==nullptr)
	return nullptr;
	auto cur=m_root;
	while(cur){
		if(cur->value>value)
		cur=cur->left;
		else if(cur->value<value)
		cur=cur->right;
		else
		return cur;
	}
	return nullptr;
}
void BinaryTree:: PreOrder(VISIT visit)const{
	//_preorder(m_root,visit);
	_preorder2(visit);
}
void BinaryTree::_preorder(TreeNode*node,VISIT visit)const{
	if(node){
	visit(node->value);
	_preorder(node->left,visit);
	_preorder(node->right,visit);
	}
}
void BinaryTree::_preorder2(VISIT visit)const{
	if(!m_root)
	return ;
	stack<TreeNode*>s;
	s.push(m_root);
	TreeNode * cur;
	while(!s.empty()){
		cur=s.top();
		s.pop();
		visit(cur->value);
		if(cur->right)//存在右节点  先把右节点放入栈中
		s.push(cur->right);
		if(cur->left)//再存放左结点 符合先序遍历
		s.push(cur->left);
	}
}
void BinaryTree:: InOrder(VISIT visit)const{
	//_inorder(m_root,visit);
	//_inorder2(visit);
	_inorder3(visit);
}
void BinaryTree::_inorder(TreeNode*node,VISIT visit)const{
	if(node){
	_inorder(node->left,visit);
	visit(node->value);
	_inorder(node->right,visit);
	}
}
void BinaryTree::_inorder2(VISIT visit)const{
	if(!m_root)
	return ;
	stack<TreeNode*>s;
	TreeNode * cur=m_root;
	while(cur||!s.empty()){//根节点不空，且栈中有值，不加前面的条件，访问不到根的右孩子
		while(cur){//找最左孩子
			s.push(cur);
			cur=cur->left;
		}
		if(!s.empty()){
			cur=s.top();//访问左孩子为空的节点
			s.pop();
			visit(cur->value);
			cur=cur->right;//转向右孩子
		}
	}
}
void BinaryTree::_inorder3(VISIT visit)const{
	if(!m_root)
	return ;
	stack<pair<TreeNode*,int> >s;
	s.push(make_pair(m_root,0));
	int times;
	TreeNode*cur;
	while(!s.empty()){
		cur=s.top().first;
		times=s.top().second;
		s.pop();
		if(times==0){//第一次压栈  左右根 先右再根再左
			if(cur->right)
			s.push(make_pair(cur->right,0))	;
			s.push(make_pair(cur,1));//第二次压栈
			if(cur->left)
			s.push(make_pair(cur->left,0));
		}
		else
		visit(cur->value);
	}
}
void BinaryTree:: PostOrder(VISIT visit)const{
	//_postorder(m_root,visit);
	_postorder2(visit);
}
void BinaryTree::_postorder(TreeNode*node,VISIT visit)const{
	if(node){
	_postorder(node->left,visit);
	_postorder(node->right,visit);
	visit(node->value);
	}
}
void BinaryTree::_postorder2(VISIT visit)const{
	if(!m_root)
	return ;
	stack<pair<TreeNode*,int> >s;
	s.push(make_pair(m_root,0));
	int times;
	TreeNode*cur;
	while(!s.empty()){
		cur=s.top().first;
		times=s.top().second;
		s.pop();
		if(times==0){//第一次压栈
			s.push(make_pair(cur,1));//第二次压栈
			if(cur->right)
			s.push(make_pair(cur->right,0))	;
			if(cur->left)
			s.push(make_pair(cur->left,0));
		}
		else
		visit(cur->value);
	}
}
void BinaryTree::Bfs(VISIT visit)const{
	if(!m_root)	
	return ;
	queue<TreeNode*>q;//先进先出
	q.push(m_root);
	TreeNode* cur;
	while(!q.empty()){
		cur=q.front();
		q.pop();
		visit(cur->value);
		if(cur->left)
		q.push(cur->left);
		if(cur->right)
		q.push(cur->right);
	}
}
int BinaryTree::LargestBST(TreeNode*& node)const{
	int Min,Max,count;
	int number=0;
	_LargestBST(m_root,Min,Max,count,number,node);
	return number;
}
bool BinaryTree::_LargestBST(TreeNode*root,int&Min,int& Max,int& count,
int& number,TreeNode*& node)const{
	count=0;
	if(!root)
	return true;
	int min1=INT_MAX,min2=INT_MAX;
	int max1=INT_MIN,max2=INT_MIN;
	int c1,c2;
	if(!_LargestBST(root->left,min1,max1,c1,number,node))//左孩子
		return false;
	if(!_LargestBST(root->right,min2,max2,c2,number,node))//右孩子
		return false;
	if((root->value<max1)||(root->value>min2))
	return false;
	count=c1+c2+1;
	Min=min(min1,root->value);
	Max=max(max2,root->value);
	if(count>number){
	number=count;
	node=root;
	}	
	return true;
}
void BinaryTree::Reverse(){
	_Reverse(m_root);
}
void BinaryTree::_Reverse(TreeNode* node){
	if(node){
		swap(node->left,node->right);
		_Reverse(node->left);
		_Reverse(node->right);
	}
}
