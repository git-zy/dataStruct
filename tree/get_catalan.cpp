#include<iostream>

using namespace std;
/*
	计算catalan数组
*/
void get_catalan(int *a,int size){
	a[0]=1;
	a[1]=1;
	int i,j,c;
	for(i=2;i<=size;i++){
		a[i]=1;
		c=0;
		for(j=0;j<i;j++)
		c+=a[j]*a[i-j-1];
		a[i]=c;
	}
}
int main(){
	const int n=19;
	int catalan[n+1];
	get_catalan(catalan,n);
	for(int i=0;i<n;i++)
	cout<<i<<": "<<catalan[i]<<endl;
}
