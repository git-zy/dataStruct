#include<iostream>
#include<algorithm>
#include<cstdlib>
using namespace std;
/*
对于长度为n的数组，求连续子数组和最接近0的值
数组 A 1,-2,3,10,-4,7,2,-5

前0项和为0
申请比a长1的空间sum[-1,0,..n-1]sum[i]是前i项的和
sum[-1]=0
sum(i-j)=sum(0-j)-sum(0-i-1);
对sum[-1,...n-1]进行排序，然后计算sum相邻元素的差的绝对值，最小值为所求
在a中任意取两个前缀子数组的和求差的最小值
*/
int min_sub(int *a,int size){
	int *sum=new int[size+1];
	sum[0]=0;
	int i;
	for(i=0;i<size;i++)
		sum[i+1]=sum[i]+a[i];
	sort(sum,sum+size+1);
	int diff=abs(sum[1]-sum[0]);//初始化
	int result=diff;
	for(i=1;i<size;i++){
		diff=abs(sum[i+1]-sum[i]);
		result=min(diff,result);
	}
	delete []sum;
	return result;
}
int main(){
	int a[]={1,-2,3,10,-4,7,2,-4};
	int m=min_sub(a,sizeof(a)/sizeof(int));
	cout<<m<<endl;
}
