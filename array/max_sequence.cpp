#include<iostream>
#include<algorithm>
using namespace std;
/*
求一个数组中 递增且连续数字最长的子数组
如 1,2,3,34,56,57,58,59,60,61,99,121
的连续为56,57,58,59,60,61
*/
int max_sequence(int *a,int size,int&from,int&to){
	//int *p=new int[size];
	int i=0,m=1;
//	for(;i<size;i++){
	//	p[i]=1;
	//}
	from=to=0;
	int begin=1; //添加一个变量记录位置就行，没有必要花费空间去记录位置
	for(i=1;i<size;i++){
		if(a[i]-a[i-1]==1){
		//p[i]+=p[i-1];
		begin+=1;
		m=max(begin,m);
		to=i;
		}
		else
		begin=1;
	}
	from=to-m+1;//最后更新的位置减长度+1
	//delete []p;
	return m;
}
int main(){
	int a[]={1,2,3,34,56,57,58,59,60,61,62,99,121};
	int from,to;
	int m=max_sequence(a,sizeof(a)/sizeof(int),from,to);
	cout<<m<<endl;
	for(int i=from;i<=to;i++)
	cout<<a[i]<<" ";
}
