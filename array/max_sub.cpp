#include<iostream>
#include<algorithm>
using namespace std;

/*
	给定一个数组a[0,...n-1]求a的连续子数组，使得该子数组的和最大
	例 1,-2,3,10,-4,7,2,-5	
	最大子数组 3,10,-4,7,2
	记s[i]为a[i]结尾的数组中和最大的子数组
	s[i+1]=max(s[i]+a[i+1],a[i+1]);  
	//如果s[i]<0 s[i+1]=a[i+1]否则为s[i+1]=s[i]+a[i+1]
	s[0]=a[0]
	动态规划  最优子数组 复杂度o(n)
*/
int max_sub(int *a,int size){
	int i;
	int sum=a[0];//当前子串的和
	int result=sum;//当前找到的最优解
	for(i=1;i<size;i++){
		if(sum<0)
		sum=a[i];
		else
		sum+=a[i];
		result=max(sum,result);
	}
	return result;
}
int sub_array(const int*a,int size,int&from,int& to){
	if(!a||(size<=0)){
	from=to=-1;
	return 0;
	}
	from=to=0;
	int i;
	int sum=a[0];
	int result=sum;
	int fromNew;//新的子数组的起点
	for(i=1;i<size;i++){
		if(sum>0)
		sum+=a[i];
		else{
			sum=a[i];//丢弃前面小于0的值
			fromNew=i;
		}
		if(result<sum){
		result=sum;
		from=fromNew;
		to=i;
		}
	}
	return result;
}
int main(){
	int a[]={1,-2,3,10,-4,7,2,-5};
	//int m=max_sub(a,sizeof(a)/sizeof(int));
	int from,to;
	int m=sub_array(a,sizeof(a)/sizeof(int),from,to);
	cout<<m<<" "<<from<<" "<<to<<endl;
}
