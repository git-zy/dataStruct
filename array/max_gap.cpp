#include<iostream>
#include<algorithm>
using namespace std;
/*
给定整数数组a[0,...n-1]求这n个数字排序后的最大间隔
如1,7,14,9,4,13  最大间隔为4
排序后  1,4,7,9,13,14  最大为4
对原数组排序 然后求后项减去前项的最大值即为解
*/
int max_gap(int *a,int size){
	sort(a,a+size);
	int m=0;
	int tmp=0;
	for(int i=1;i<size;i++){
		tmp=a[i]-a[i-1];
		m=max(tmp,m);
	}
	return m;
}
int main(){
	int a[]={1,7,14,9,4,13};
	int m=max_gap(a,sizeof(a)/sizeof(int));
	cout<<m<<" "<<endl;
}
