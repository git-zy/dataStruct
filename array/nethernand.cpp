#include<iostream>
#include<algorithm>

using namespace std;
/*
红 白 蓝 三色的小球乱序排列在一起，重新排列这些小球使得
红白蓝三色的同颜色在一起
将 红白蓝映射为012 给定数组a[0,...n-1]
数组中的元素只能取0 1 2 使数组排列为00..0..11..1..2..2的形式

借鉴快速排序的思想
定义三个指针 
begin=0  current=0,end=n-1
a[cur]==2 则a[cur]与a[end]交换end-- cur不变
a[cur]==1 cur++ begin不变end不变
a[cur]==0 
	若begin==cur; begin++,cur++
	若begin!=cur; a[cur]与a[begin]交换，begin++ cur不变
优化：
	[begin,current)中不存在2，
	a[begin]==0或者 a[begin]==1 不可能等于2
	若beign!=cur  则a[begin]==1
因此 当a[cur]==0时
	若begin!=cur  a[begin==1]
	交换后a[cur]==1  则直接cur++;
*/
/*
循环不变式：某命题初始为真，且每次更改后仍然为真，则若干次更改后命题仍然为真
三个变量 begin end cur 将数组分为4个区域
[0,begin) 所有数据都是0
[begin,cur)所有数据都是1
[end,size-1)所有数据都是2
[cur,end] 未知
循环不变式
初值begin=cur=0,end=size-1,前3个区间都为空集，满足上述条件
变量cur，根据a[cur] 的值做相应处理，即扩充相应的区间，直到区间[cur,end)为空
即cur==end时退出
*/

void nerther(int *a,int size){
	int begin,cur,end;
	begin=cur=0;
	end=size-1;
	while(cur<=end){
		if(a[cur]==2){
			swap(a[cur],a[end]);
			end--;
		}
		else if(a[cur]==1)
		cur++;
		else if(a[cur]==0){
			if(begin==cur)
				begin++,cur++;
			else{
				swap(a[cur],a[begin]);
				begin++;
				}
		}
	}
}
void nerther1(int *a,int size){
	int begin,cur,end;
	begin=cur=0;
	end=size-1;
	while(cur<=end){
		if(a[cur]==2){
			swap(a[cur],a[end]);
			end--;
		}
		else if(a[cur]==1)
		cur++;
		else if(a[cur]==0){
/*			if(begin==cur)
				begin++,cur++;
			else{
				swap(a[cur],a[begin]);
				begin++;
				cur++;
				}*/
//			可以简化为
		if(begin!=cur)
			swap(a[cur],a[begin]);
		begin++,cur++;
		}
	}
}
void nerther2(int *a,int size){
	int begin,cur,end;
	begin=0;
	cur=end=size-1;
	while(cur>=begin){
		if(a[cur]==2){
			swap(a[cur],a[end]);
			end--;
			cur--;
		}
		else if(a[cur]==1)
		cur--;
		else if(a[cur]==0){
		swap(a[begin],a[cur]);
		begin++;
	}
}
}
int main(){
	int a[]={1,2,1,1,2,1,2,0,0,0,2,1};
	nerther2(a,sizeof(a)/sizeof(int));
	//sort(a,a+sizeof(a)/sizeof(int));
	for(int i=0;i<sizeof(a)/sizeof(int);i++){
		cout<<a[i]<<" ";
	}
	cout<<endl;
}
