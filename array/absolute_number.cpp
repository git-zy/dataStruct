#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;
/*
	给定n个数，出现次数最多的称为众数，若某个众数出现的次数大于n/2
	称该众数为绝对众数,最多只有1个
a {1,2,1,3,2} 1 2 都是众数但偶不是绝对众数
a {1,2,1,3,1} 1是绝对众数
已知给定的n个整数中存在绝对众数，以最低的时空复杂度计算该绝对众数
删除数组中两个不同的数，绝对众数不变
若两个数中有一个是绝对众数，则剩余的n-2个数字中绝对众数仍然大于(n-2)/2
若两个数中没有绝对众数，则显然不影响绝对众数

对这n个数进行排序，则第n/2个数就是绝对众数

设m为候选绝对众数，出现次数为c初始化为0
遍历数组A
c==0 m=a[i]
若c!=0且m!=a[i] 则同时删掉m和a[i]  删掉m就是m-1 删掉a[i]把数组的最后一个数字拿过来
若c!=0且m==a[i]，则c++
*/
int find_num(int *a,int size){//O(n)
	int m=a[0],c=0;
	for(int i=0;i<size;i++){
		if(c==0){
			m=a[i];
			c++;
		}
		else if(m!=a[i])
		c--;//次数直接减1
		else //相等计数值加1
		c++;
	}
	return m;
}
int find_sort(int *a,int size){//O(nlogn)
	sort(a,a+size);
	return a[size/2];
}
/*
给定n个整数，查找出现次数超过n/3的所有可能的数  最多有2个
有可能这样的数字不存在
时间复杂度为O(n)空间为O(1)
删除3个不相同的数，类似于上面的思想
使用cm记录候选值m出现的次数，若cm为0则重新选择候选值

*/
void  find_thr(const int *a,int size,vector<int>&vec){
	int m,n;
	int cm=0,cn=0;
	int i;
	for(i=0;i<size;i++){
		if(cm==0){
		m=a[i];
		cm++;
		}
		else if(cn==0){
		n=a[i];
		cn++;
		}
		else if(a[i]==m)
		cm++;
		else if(a[i]==n)
		cn++;
		else 
		cm--,cn--;
	}//这里结束如果存在众数，则m n就是
	cm=0,cn=0;
	for(i=0;i<size;i++){
		if(a[i]==m)
		cm++;
		else if(a[i]==n)
		cn++;
	}
	if(cm>size/3)
	vec.push_back(m);
	if(cn>size/3)
	vec.push_back(n);
}
int main(){
	/*
	n/2  众数
	int a[]={8,8,1,1,1,8,1,1,6,1,8};
	int m=find_sort(a,sizeof(a)/sizeof(int));
	int m=find_num(a,sizeof(a)/sizeof(int));
	cout<<m<<endl;*/
	// n/3众数  
	int a[]={1,2,3,2,5,2,2,3,3,2,3};
	vector<int>vec;
	find_thr(a,sizeof(a)/sizeof(int),vec);
	if(!vec.empty())
	for(const auto v:vec)
	cout<<v<<" ";
	cout<<endl;
}
