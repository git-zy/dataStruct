#include<iostream>
#include<algorithm>

using namespace std;
/*
给定一个数组找到从1开始第一个不在数组中的正整数
3,5,1,2,-3,7,14,8  则数字为4
1.直接从1开始遍历在不在数组中 o(n) n个数字需要n次 总共o(n^2)
2.先排序  o(nlogn)  遍历每个数需要n次  每次o(logn) 共O(nlogn)
3.开辟一个等长的数组 在这个数组范围内的数字对应位置置为1 o(n) 
 放置完成后遍历1次，总共需要o(n)
4.
若a[i]=i,i+1,继续比较后面的元素
若a[i]<i或a[i]>n或 a[a[i]]=a[i]丢弃a[i]
若a[i]>i,则将a[a[i]]和a[i]交换

快速丢弃a[i]  将a[n]值赋值给a[i],n-1
*/
int first_miss(int *a,int size){
	a--;//从1开始计数
	int i=1;
	while(i<=size){
		if(a[i]==i){
		i++;
		}
		else if(a[i]<i||a[i]>=size||a[a[i]]==a[i]){
		a[i]=a[size];
		size--;
		}
		else if(a[i]>i)
		swap(a[a[i]],a[i]);
	}
	return i;
}
int main(){
	int a[]={3,5,1,2,6,7,-4,8};
	int m=first_miss(a,sizeof(a)/sizeof(int));
	cout<<m<<endl;
}
