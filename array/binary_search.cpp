#include<iostream>
#include<cstdlib>

using namespace std;

int find_num(int *a,int size,int value){
	int begin=0;
	int end=size-1;
	bool flag=false;
	int mid;
	while(begin<=end){
	  mid=begin+(end-begin)/2;
		if(a[mid]==value){
			flag=true	;
			break;
		}
		if(a[mid]>value)
		end=mid-1;
		else
		begin=mid+1;
	}
	if(flag)
	return a[mid];
	return -1;
}
int main(int argc,char**argv){
	int a[]={1,2,3,4,7,8,9,10};
	cout<<find_num(a,sizeof(a)/sizeof(int),atoi(argv[1]))<<endl;
}
