#include<iostream>
#include<queue>
/*对一个有向无环图G进行排序将G中所有顶点排成线性序列
使得图中任意一堆顶点u、v若边(u,v)属于集合G则在线性序列中
u出现在v的前面*/
/*
方法：
从有向图中选择一个没有前驱(即入度为0)的顶点并且输出它
从网中删除该顶点并且删除从该顶点出发的全部有向边
重复上述  直到剩余的网中不在有没有前驱的顶点为止
*/
//节点数为n，用邻接矩阵graph[n][n]存储边权 
//i行到j列存在有向边 则存储为1  j列为该节点的入度  i行为该节点的出度
//用indegree[n]存放每个节点的入度
using namespace std;
#define n 8
int graph[n][n]={
	{0,1,0,1,0,0,0,0},
	{0,0,1,0,0,0,0,0},
	{0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0},
	{0,0,0,0,0,1,0,0},
	{0,0,1,1,0,0,0,0},
	{0,0,1,0,0,0,0,0},
	{0,0,0,0,0,0,1,0}};
int indegree[n]={0,1,3,2,0,1,1,0};
void topologic(int *toposort){//如果存在环则结果的输出达不到节点的个数
	int cnt=0;//当前拓扑排序表中有多少个节点
	queue<int>q;//保存入度为0的节点，可以使用栈，或者随机取
	int i;
	for(i=0;i<n;i++){
		if(indegree[i]==0)
		q.push(i);//先把入度为0的节点放在队列
	}
	int cur;//当前入度为0的节点
	while(!q.empty()){
		cur=q.front();
		q.pop();
		toposort[cnt++]=cur;
		for(i=0;i<n;i++){
			if(graph[cur][i]!=0){//当前列不为0
				indegree[i]--;
				if(indegree[i]==0)
				q.push(i);
			}
		}
	}
}
int main(){
	int a[n]={0};
	topologic(a);
	for(int i=0;i<n;++i)
	cout<<a[i]<<" ";
	
}
