#include"queue.h"

int main(){
	try{
		Queue<int>q;
		for (int i = 0; i < 5; i++)
			q.Enter(i);
		q.Print();
		q.Leave();
		q.Enter(10);
		q.Enter(12);
		q.Enter(11);
		q.Print();
		std::cout << q.First() << "\n";
		std::cout << q.Size() << "\n";
		q.Leave();
		q.Leave();
		q.Leave();
		q.Leave();
		q.Print();
		std::cout << q.Size() << "\n";
	}
	catch (QueueEmptyExceprion& e){
		e.Reason();
	}
	system("pause");
	return 0;
}