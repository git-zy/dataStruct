#ifndef QUEUE_H
#define QUEUE_H

#include<iostream>
#include <cstdlib>

class QueueEmptyExceprion{
public:
	QueueEmptyExceprion operator()(){
		
	}
	void Reason(){
		std::cout << " The queue is empty no data to delete\n";
	}
};
template <typename T>
struct Node{
	Node(const T&rhs):next(nullptr),value(rhs){}
	T& GetValue(){ return value; }
	void SetValue(const T&rhs){ value = rhs; }
	Node<T>* GetNode(){ return next; }
	void SetNode(Node<T>*rhs){ next = rhs; }
private:
	struct Node<T>* next;
	T value;
};
template <typename T>
class Queue{
public:
	Queue():head(nullptr),tail(nullptr),num(0){}
	~Queue();
	bool Empty(){ return head == nullptr; }
	void Enter(const T& value);
	void Leave();
	void Print();
	T& Size(){ return num; }
	T& First(){
		if (Empty())
			throw QueueEmptyExceprion();
		return head->GetValue();
	}
private:
	
	Node<T> *head;
	Node<T> *tail;
	int num;
};
template <typename T>
Queue<T>::~Queue(){
	while (!Empty())
		Leave();
}
template <typename T>
void Queue<T>::Enter(const T& value){
	Node<T>* new_node = new Node<T>(value);
	if (Empty()){
		head = tail = new_node;//为空的时候把头结点的值指向这个节点
	}
		
	else{
		tail->SetNode(new_node);//尾部插入元素
		tail = new_node;//使尾指针指向新插入的节点
	}
	++num;
}
template <typename T>
void Queue<T>::Print(){
	auto cur = head;
	while (cur){
		std::cout << cur->GetValue() << " ";
		cur = cur->GetNode();
	}
	std::cout << std::endl;
}
template <typename T>
void Queue<T>::Leave(){
	if (Empty())
		throw QueueEmptyExceprion();
	else{
		auto pre = head;
		head=head->GetNode();
		delete pre;
	}
	--num;
}
#endif