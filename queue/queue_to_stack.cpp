#include <iostream>
#include<queue>
 
using namespace std;
class MyStack{
	private:
	queue<int>q1;
	queue<int>q2;
	public:
	void push (int value){
		q1.push(value)	;
	}
	void pop();
	bool empty(){return q1.empty();}
	int size(){return q1.size();}
	int top(){return q1.back();}
};
void MyStack::pop(){
	int temp;
	if(q1.empty())
	return ;
	else{
	while(q1.size()!=1){
		int temp=q1.front();
		q2.push(temp);
		q1.pop();
	}
	temp=q1.front();
	q1.pop();
	while(!q2.empty()){
		int t=q2.front();
		q1.push(t);
		q2.pop();
	}
	}
}
void test(){
	MyStack s;
	for(int i=0;i<4;i++)
	s.push(i);
	cout<<"s.top="<<s.top()<<endl;
	cout<<"s.size"<<s.size()<<endl;
	s.pop();
	cout<<"s.top="<<s.top()<<endl;
	s.pop();
	s.pop();
	s.push(22);
	cout<<"s.top="<<s.top()<<endl;
	cout<<"s.size"<<s.size()<<endl;
	s.pop();
	cout<<"s.empty()"<<s.empty()<<" s.size()"<<s.size()<<endl;
	
}
int main(){
	test();
}
