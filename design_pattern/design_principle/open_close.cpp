#include <iostream>
using namespace std;
//开闭原则  对扩展开放 对修改关闭  增加功能是通过增加代码实现，不是通过修改代码实现
class AbstractCalculator{
public:
	virtual int getResult()=0;
	virtual void setNumber(int ,int)=0;
};
//加法类
class Add:public AbstractCalculator{
public:
	void setNumber(int m,int n){
	a=m; b=n;
	}
	virtual int getResult(){
		return a+b;
}
private:
	int a;
	int b;
};
class Mul:public AbstractCalculator{
public:
	void setNumber(int m,int n){
	a=m; b=n;
	}
	virtual int getResult(){
		return a*b;
	}
private:
	int a;
	int b;
};
//再增加其他运算类只需要增加代码
void test(){
	AbstractCalculator*calc=new Add;
	calc->setNumber(10,20);
	cout<<"add result is: "<<calc->getResult()<<endl;
	delete calc;
	calc=new Mul;
	calc->setNumber(10,20);
	cout<<"mul result is: "<<calc->getResult()<<endl;
	delete calc;
}
int main(){
	test();
}
