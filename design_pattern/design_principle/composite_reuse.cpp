#include <iostream>
using namespace std;

//合成复用原则  多组合少继承
class AbstractCar{
public:
	virtual void run()=0;
};

class Aodi:public AbstractCar{
public:
	virtual void run(){
		cout<<" Aodi run...\n";
	}
};
class Cadilac:public AbstractCar{
public:
	virtual void run(){
		cout<<" Cadilac run...\n";
	}
};
#if 0
//少继承
class PersonA:public Aodi{
public:
	void go(){
		run();
	}
};
class PersonB:public Cadilac{
public:
	void go(){
		run();
	}
};
#endif
//多组合
class Person{
public:
	void setCar(AbstractCar* car){
		mCar=car;
	}
	void go(){	
		mCar->run();
	}
	~Person(){
		if(mCar!=nullptr)
		delete mCar;
	}
private:
AbstractCar *mCar;
};
void test(){
	Person *p=new Person;
	p->setCar(new Aodi);
	p->go();
	p->setCar(new Cadilac);
	p->go();
	delete p;
}
//继承与组合优先使用组合，类之间的耦合度低
int main(){
	test();
}
