#include<iostream>
//依赖倒转原则  针对接口编程，不要针对具体实现编程
using namespace std;

class BandWorker{
public:
	void saveService(){
		cout<<"存款业务\n";
	}
	void payService(){
		cout<<"支付业务\n";
	}
	void transferService(){
		cout<<"转账业务\n";
	}
};
//中层模块
void doSave(BandWorker* worker){
	worker->saveService();
}
void doPay(BandWorker* worker){
	worker->payService();
}
void doTransfer(BandWorker* worker){
	worker->transferService();
}
void test1(){ //依赖于中间层的具体实现
	BandWorker* worker=new BandWorker;
	doTransfer(worker);
	doPay(worker);
	doSave(worker);
	delete worker;
}
class AbstractWorker{
public:
	virtual void doBusiness()=0;
};
//单一职责
class saveBankWorker:public AbstractWorker{
public:
	virtual void doBusiness(){
		cout<<"存款业务\n";
	}
};
class payBankWorker:public AbstractWorker{
public:
	virtual void doBusiness(){
		cout<<"支付业务\n";
	}
};
class transferBankWorker:public AbstractWorker{
public:
	virtual void doBusiness(){
		cout<<"转账业务\n";
	}
};
//新的中间层依赖于抽象层
void doNewBusiness(AbstractWorker* worker){
	worker->doBusiness();
}
void test2(){
	AbstractWorker* pay=new payBankWorker;
	doNewBusiness(pay);
}
int main(){
//test1();
	test2();
}
