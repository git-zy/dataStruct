#include<iostream>
#include<string>
#include<vector>

using namespace std;
//最少知识法则  不要把自己的设计的类全部暴露，提供中间层接口
class AbstractBuilding{
public:
	virtual void sale()=0;
	virtual string getQuilty()=0;
};
class BuildingA:public AbstractBuilding{
public:
	BuildingA():mQuilty("high_quilty"){}
	virtual void sale(){
		cout<<"buildingA "<<mQuilty<<" has been saled!!!\n";
	}
	virtual string getQuilty(){
	return mQuilty;
	}
private:
	string mQuilty;
};
class BuildingB:public AbstractBuilding{
public:
	BuildingB():mQuilty("low_quilty"){}
	virtual void sale(){
		cout<<"buildingB "<<mQuilty<<" has been saled!!!\n";
	}
	virtual string getQuilty(){
	return mQuilty;
	}
private:
	string mQuilty;
};
//中介类
class Media{
public:
	Media(){
		AbstractBuilding* building=new BuildingA;
		vec.push_back(building);
		building=new BuildingB;
		vec.push_back(building);
	}
	~Media(){
		for (auto it=vec.begin();it!=vec.end();++it)
		if(*it!=nullptr)
		delete *it;
	}
	//对外提供接口
	AbstractBuilding* findMyBuilding(const string& quilty){
		for(auto it=vec.begin();it!=vec.end();++it){
		if((*it)->getQuilty()==quilty)
		return *it;
		}
		return nullptr;
	}
private:
	vector<AbstractBuilding*>vec;
};
void test(){
	Media *m=new Media;
	//auto ret=m->findMyBuilding("high_quilty");
	auto ret=m->findMyBuilding("low_quilty");
	if(ret!=nullptr)
	ret->sale();
	else
	cout<<"no suitbale building for you\n";
	delete m;
}
int main(){
	test();
}
