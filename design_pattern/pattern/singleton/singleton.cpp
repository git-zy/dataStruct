#include<iostream>

/*
单例模式 这个系统中只能有一个A类型的对象
1.构造函数私有
2.增加静态私有的当前类的指针变量
3.提供静态对外接口,可以让用户获得单例对象
懒汉式线程不安全  违背了单例的原则，需要自己加锁处理保证
if(instance==NULL){
	lock()...
	if(instance==NULL)
	instance=new Singleton();
}
return instance
双重锁定是有必要的,当instance存在时可以直接返回
当instance为空的时候，且同时有两个线程同时调用getInstance时都将通过第一重判断
如果没有第二层即使加锁了都会继续创建新的实例
饿汉式是线程安全的，全局的静态变量在main函数启动之前就已经创建好了，它返回的是
全局唯一的一个对象
*/
using namespace std;
//单例 懒汉式  需要的时候才创建
class Singleton_lazy{
private:
	Singleton_lazy(){
		cout<<"this is lazy\n";
	}
	static Singleton_lazy* pLazy;
public:
	static Singleton_lazy* getInstance(){
		if(pLazy==nullptr)
			pLazy=new Singleton_lazy;
		return pLazy;
	}
};
Singleton_lazy*Singleton_lazy::pLazy=nullptr;

//单例 饿汉式  在main函数开始之前已经创建好
class Singleton_hungry{
private:
	Singleton_hungry(){
		cout<<"this is hungry\n";
	}
	static Singleton_hungry* pHungry;
public:
	static Singleton_hungry*getInstance(){
		return pHungry;
	}
#if 0
//不能提供这个方法，不知道是否那个会调用
	static void freeSpace (){
		if(pHungry!=NULL)
			delete pHungry;
	}
#endif
};

Singleton_hungry* Singleton_hungry::pHungry=new Singleton_hungry;
void test(){
	auto p1=Singleton_lazy::getInstance();
	auto p2=Singleton_lazy::getInstance();
	if(p1==p2)
	cout<<"单例模式\n";
	else 
	cout<<"不是单例模式\n";
	auto p3=Singleton_hungry::getInstance();
	auto p4=Singleton_hungry::getInstance();
	if(p3==p4)
	cout<<"单例模式\n";
	else 
	cout<<"不是单例模式\n";
}
//单例对象释放问题 不用考虑因为全局只有一个单例对象

int main(){
	cout<<"this is main\n";
	test();
}
