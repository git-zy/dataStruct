#include<iostream>
#include<queue>
#include<unistd.h>
using namespace std;
/*
	类比烧烤店 有服务员可以记录顾客的各种需求，然后服务员发送
	各种烧烤的请求给烧烤者
	客户端 发送不同的数字编号对应不同的请求
	服务器 解析编号，并执行请求
*/
//协议处理类
class HandleProtocol{
public:
	//处理增加金币
	void AddMoney(){
		cout<<"给玩家加金币\n";
	}
	//处理玩家钻石
	void AddDiamond(){
		cout<<"给玩家加钻石\n";
	}
	//处理玩家装备
	void AddEquipment(){
		cout<<"给玩家加装备\n";
	}
	//处理玩家升级
	void AddLevel(){
		cout<<"给玩家升级\n";
	}
};
//命令接口
class AbstractCommand{
public:
	virtual void handle()=0;//处理客户端请求的接口
};
//处理增加金币请求
class AddMoneyCommand:public AbstractCommand{
public:
	AddMoneyCommand(HandleProtocol* protocol){
		p=protocol;
	}
	virtual void handle(){
		p->AddMoney();
	}
private:
	HandleProtocol* p;
};
//处理增加钻石请求
class AddDiamondCommand:public AbstractCommand{
public:
	AddDiamondCommand(HandleProtocol* protocol){
		p=protocol;
	}
	virtual void handle(){
		p->AddDiamond();
	}
private:
	HandleProtocol* p;
};
//处理增加装备请求
class AddEquipmentCommand:public AbstractCommand{
public:
	AddEquipmentCommand(HandleProtocol* protocol){
		p=protocol;
	}
	virtual void handle(){
		p->AddEquipment();
	}
private:
	HandleProtocol* p;
};
//处理增加金币请求
class AddLevelCommand:public AbstractCommand{
public:
	AddLevelCommand(HandleProtocol* protocol){
		p=protocol;
	}
	virtual void handle(){
		p->AddLevel();
	}
private:
	HandleProtocol* p;
};

class Server{
public:
	void addRequest(AbstractCommand*command){
		commands.push(command);
	}
	void startHandle(){
		while(!commands.empty()){
		sleep(2);
		auto c=commands.front();
		c->handle();
		commands.pop();
		}
	}
private:
	queue<AbstractCommand*> commands;
};
void test(){
	HandleProtocol* protocol= new HandleProtocol;
	//客户端增加金币
	AddMoneyCommand* addmoney=new AddMoneyCommand(protocol)	;
	//客户端增加钻石
	AddDiamondCommand* adddiamond=new AddDiamondCommand(protocol)	;
	//客户端增加装备
	AddEquipmentCommand* addequipment=new AddEquipmentCommand(protocol)	;
	//客户端增加等级
	AddLevelCommand* addlevel=new AddLevelCommand(protocol)	;
	
	Server* server=new Server;
	server->addRequest(addmoney);
	server->addRequest(adddiamond);
	server->addRequest(addequipment);
	server->addRequest(addlevel);
	
	server->startHandle();
	delete protocol;
	delete addmoney;
	delete adddiamond;
	delete addequipment;
	delete addlevel;
	delete server;
}
int main(){

	test();
}
