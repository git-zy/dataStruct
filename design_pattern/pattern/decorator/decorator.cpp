#include<iostream>

using namespace std;
/*
一般情况 使用继承实现类的功能拓展
装饰模式:动态的给一个对象添加一些额外的职责，就增加功能来说
装饰模式比生成子类更灵活
*/

class AbstractHero{
public:
	virtual void ShowStatus()=0;
public:
	int mHp;
	int mMp;
	int mAt;
	int mDf;
};
class HeroA:public AbstractHero{
public:
	HeroA(){
	mHp=0;
	mMp=0;
	mAt=0;
	mDf=0;
	}
virtual void ShowStatus(){
	cout<<"血量 "<<mHp<<endl;
	cout<<"魔法 "<<mMp<<endl;
	cout<<"攻击 "<<mAt<<endl;
	cout<<"防御 "<<mDf<<endl;

	}
};
//英雄穿上某个装饰符，他不是原来的英雄，但他还是英雄
class AbstractEquipment:public AbstractHero{
public:
	AbstractEquipment(AbstractHero* hero){
		pHero=hero;
	}
	virtual void ShowStatus(){}
public:
AbstractHero* pHero;
};
//狂徒铠甲
class KuangtuEquipment:public AbstractEquipment{
public:
	KuangtuEquipment(AbstractHero*hero):AbstractEquipment(hero){}
	//增加额外功能
	void addKuangtu(){
		cout<<"英雄穿上狂徒之后...\n";
		mHp=pHero->mHp+3000;
		mMp=pHero->mMp;
		mAt=pHero->mAt;
		mDf=pHero->mDf;
		delete pHero;
	}
	virtual void ShowStatus(){
	addKuangtu();
	cout<<"血量 "<<mHp<<endl;
	cout<<"魔法 "<<mMp<<endl;
	cout<<"攻击 "<<mAt<<endl;
	cout<<"防御 "<<mDf<<endl;
	}
};
//无尽之刃
class WujinEquipment:public AbstractEquipment{
public:
	WujinEquipment(AbstractHero*hero):AbstractEquipment(hero){}
	//增加额外功能
	void addWujin(){
		cout<<"英雄买了无尽之后...\n";
		mHp=pHero->mHp;
		mMp=pHero->mMp;
		mAt=pHero->mAt+1000;
		mDf=pHero->mDf;
		delete pHero;
	}
	virtual void ShowStatus(){
	addWujin();
	cout<<"血量 "<<mHp<<endl;
	cout<<"魔法 "<<mMp<<endl;
	cout<<"攻击 "<<mAt<<endl;
	cout<<"防御 "<<mDf<<endl;
	}
};
void test(){
	AbstractHero*hero=new HeroA;
	hero->ShowStatus();
	cout<<"----------\n";
	hero=new KuangtuEquipment(hero);
	cout<<"----------\n";
	hero->ShowStatus();
	hero=new WujinEquipment(hero);
	cout<<"----------\n";
	hero->ShowStatus();
	
}
int main(){
	test();
}
