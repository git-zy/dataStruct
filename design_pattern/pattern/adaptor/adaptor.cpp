#include<iostream>
#include<vector>
#include<algorithm>
#include<stdlib.h>
using namespace std;
/*
适配器模式:将一个类的接口转换成客户希望的另外一个接口，Adaptor使得
原本由于接口不兼容而不能一起工作的类可以一起工作
(类比姚明去NBA需要翻译 在CBA不需要),姚明刚开始去NBA不会英语，短时间学不会
，队友教练学不会中文，只能通过翻译,翻译就是适配器
充电器的例子220v转5v,树的插入时我提供的接口是一个参数的，但是内部有个函数需要
2个参数，这个对外的统一接口就是适配器
*/

struct MyPrint{
	void operator()(int v1,int v2){
		cout<<v1+v2<<endl;
	}
};
//定义目标接口，适配成一个参数的
class Target{
public:
	virtual void operator()(int v)=0;
};
//适配器
class Adaptor:public Target{
public:
	Adaptor(int value):val(value){}
	virtual void operator()(int v){
		p (v,val);
	}
private:
MyPrint p;
int val;
};
Adaptor MyBind(int value){
	return Adaptor(value);
}
int main(int argc,char**argv){
	if(argc!=2)
	cerr<<"neea a command line param\n";
	vector<int>v ;
	for(int i=0;i<10;i++)
	v.push_back(i);
	for_each(v.begin(),v.end(),MyBind(atoi(argv[1])));
}
