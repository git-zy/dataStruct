#include<iostream>

using namespace std;
/*
策略模式:定义了算法家族，分别封装起来，让他们之间可以相互替换，此模式
让算法的变换，不会影响到使用算法的用户
*/
class WeaponStrategy{
public:
	virtual void UseWeapon()=0;
};

class Knife:public WeaponStrategy {
public: 
	virtual void UseWeapon(){
		cout<<"use knife\n";
	}
};
class AK47:public WeaponStrategy{
public:
	virtual void UseWeapon(){
		cout<<"use AK47\n";
	}
};
class Person{
public:
	void setWeapon(WeaponStrategy* weapon){
		pWs=weapon;
	}
	void ThrowWeapon(){
		pWs->UseWeapon();
	}
private:
WeaponStrategy* pWs;
};
void test(){
	Person	*p=new Person;
	p->setWeapon(new Knife);
	p->ThrowWeapon();
	p->setWeapon(new AK47);
	p->ThrowWeapon();
	delete p;
}
int main(){
	test();
}
