#include<iostream>

using namespace std;
/*
模板方法模式:定义一个操作中算法的框架，将一些步骤延迟到子类中
(类比 冲咖啡  泡茶	
步骤类似 
1 煮水  2 冲泡咖啡 3.倒入杯中 4 加糖和牛奶
1 煮水  2 冲泡茶叶 3.倒入杯中 4 加柠檬
)
*/
class Template{
public:
	//定步骤
	virtual void BoilWater()=0;
	virtual void Brew()=0;
	virtual void Pull()=0;
	virtual void AddSth()=0;
	//模板方法
	void make(){
		BoilWater();
		Brew();
		Pull();
		AddSth();
	}
};
class Coffee:public Template{
	//实现具体步骤
	virtual void BoilWater(){
		cout<<"煮水\n";
	}
	virtual void Brew(){
		cout<<"泡咖啡\n";
	}
	virtual void Pull(){
		cout<<"倒入杯中\n";
	}
	virtual void AddSth(){
		cout<<"加糖和牛奶\n";
	}
};
class Tea:public Template{
	//实现具体步骤
	virtual void BoilWater(){
		cout<<"煮水\n";
	}
	virtual void Brew(){
		cout<<"泡茶\n";
	}
	virtual void Pull(){
		cout<<"倒入杯中\n";
	}
	virtual void AddSth(){
		cout<<"加柠檬\n";
	}
};
void test(){
	Template* t;
	t=new Tea;
	t->make();
	delete t;
	t=new Coffee;
	t->make();
	delete t;
}
int main(){
	test();
}
