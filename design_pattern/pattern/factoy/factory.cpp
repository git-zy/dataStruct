#include<iostream>
//工厂方法模式=简单工厂模式+开闭原则
//优点 扩展简单  缺点  类的个数成倍增加，维护难抽象个数增多
//适用场景 
/*
1.客户端不知道它所需要的对象的类
2.抽象工厂通过其子类来指定创建哪个对象
*/
using namespace std;
//抽象水果类
class AbstractFruit{
public:
	virtual void showName()=0;
};

class Apple:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is an apple\n";
	}
};
class Banana:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is a banana\n";
	}
};
class Pear:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is a pear\n";
	}
};
//抽象工厂
class AbstractFruitFactory{
public:
	virtual AbstractFruit* createFruit()=0;	
};
//苹果工厂类
class AppleFactory:public AbstractFruitFactory{
public:
	virtual AbstractFruit* createFruit(){
		return new Apple;
	}
};
//香蕉工厂类
class BananaFactory:public AbstractFruitFactory{
public:
	virtual AbstractFruit* createFruit(){
		return new Banana;
	}
};
//梨子工厂类
class PearFactory:public AbstractFruitFactory{
public:
	virtual AbstractFruit* createFruit(){
		return new Pear;
	}
};
void test(){
	AbstractFruitFactory*fac=NULL;
	AbstractFruit*fruit=NULL;
	//创建苹果工厂
	fac=new AppleFactory;
	fruit=fac->createFruit();
	fruit->showName();
	delete fac;
	delete fruit;
	//创建香蕉工厂
	fac=new BananaFactory;
	fruit=fac->createFruit();
	fruit->showName();
	delete fac;
	delete fruit;
}
int main(){
	test();
}
