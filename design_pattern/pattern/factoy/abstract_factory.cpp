#include<iostream>

using namespace std;
//抽象工厂针对的是产品族而不是产品等级

//抽象苹果
class AbstractApple{
public:
	virtual void showName()=0;
};
class ChinaApple:public AbstractApple{
public:
	virtual void showName(){
		cout<<"china apple\n";
	}
};
class USAApple:public AbstractApple{
public:
	virtual void showName(){
		cout<<"USA apple\n";
	}
};
class JapanApple:public AbstractApple{
public:
	virtual void showName(){
		cout<<"japan apple\n";
	}
};
class AbstractBanana{
public:
	virtual void showName()=0;
};
class ChinaBanana:public AbstractBanana{
public:
	virtual void showName(){
		cout<<"china banana\n";
	}
};
class USABanana:public AbstractBanana{
public:
	virtual void showName(){
		cout<<"USA Banana\n";
	}
};
class JapanBanana:public AbstractBanana{
public:
	virtual void showName(){
		cout<<"japan banana\n";
	}
};
class AbstractPear{
public:
	virtual void showName()=0;
};
class ChinaPear:public AbstractPear{
public:
	virtual void showName(){
		cout<<"china pear\n";
	}
};
class USAPear:public AbstractPear{
public:
	virtual void showName(){
		cout<<"USA pear\n";
	}
};
class JapanPear:public AbstractPear{
public:
	virtual void showName(){
		cout<<"japan Pear\n";
	}
};
//抽象工厂 针对产品族
class AbstractFactory{
public:
	virtual AbstractApple* createApple()=0;
	virtual AbstractBanana* createBanana()=0;
	virtual AbstractPear* createPear()=0;
};
//中国工厂
class ChinaFactory:public AbstractFactory{
	virtual AbstractApple*createApple(){
		return new ChinaApple;
	}
	virtual AbstractBanana* createBanana(){
		return new ChinaBanana;
	}
	virtual AbstractPear* createPear(){
		return new ChinaPear;
	}
};
class USAFactory:public AbstractFactory{
	virtual AbstractApple*createApple(){
		return new USAApple;
	}
	virtual AbstractBanana* createBanana(){
		return new USABanana;
	}
	virtual AbstractPear* createPear(){
		return new USAPear;
	}
};
class JapanFactory:public AbstractFactory{
	virtual AbstractApple*createApple(){
		return new JapanApple;
	}
	virtual AbstractBanana* createBanana(){
		return new JapanBanana;
	}
	virtual AbstractPear* createPear(){
		return new JapanPear;
	}
};
void test(){
	AbstractFactory *fac=NULL;
	AbstractApple *apple=NULL;
	AbstractBanana* banana=NULL;
	AbstractPear*pear=NULL;

	fac=new ChinaFactory;
	apple=fac->createApple();
	apple->showName();
	banana=fac->createBanana();
	banana->showName();
	pear=fac->createPear();
	pear->showName();
	delete fac;
	delete apple;
	delete banana;
	delete pear;	
	fac=new JapanFactory;
	apple=fac->createApple();
	apple->showName();
	banana=fac->createBanana();
	banana->showName();
	pear=fac->createPear();
	pear->showName();
	delete fac;
	delete apple;
	delete banana;
	delete pear;	
	fac=new USAFactory;
	apple=fac->createApple();
	apple->showName();
	banana=fac->createBanana();
	banana->showName();
	pear=fac->createPear();
	pear->showName();
	delete fac;
	delete apple;
	delete banana;
	delete pear;	
}
int main(){
	test();
}
