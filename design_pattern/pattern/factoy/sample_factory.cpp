#include<iostream>
//简单工厂模式不符合开闭原则,增加新功能通过修改代码实现
//且工厂类的职责过重  工厂不符合开闭原则但是原来的水果类是复合的
//优点 应用和具体的实现解耦 通过工厂完成这些
//适用场景  
/*
1.工厂类负责创建的对象较少,由于创建的对象较少,不会造成工厂方法中的业务逻辑太复杂
2.客户端只知道传入工厂的参数,不关心如何创建对象
*/
using namespace std;
//抽象水果类
class AbstractFruit{
public:
	virtual void showName()=0;
};

class Apple:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is an apple\n";
	}
};
class Banana:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is a banana\n";
	}
};
class Pear:public AbstractFruit{
public:
	virtual void showName(){
		cout<<"this is a pear\n";
	}
};
//根据传入的参数返回对应的对象
class FruitFactory{
public:
	static AbstractFruit* createFruit(const string name){
		if(name=="apple")
		return new Apple;
		else if(name=="banana")
		return new Banana;
		else if(name=="pear")
		return new Pear;
		else 
		return NULL;
	}
};
void test(){
	FruitFactory* fac=new FruitFactory;
	AbstractFruit* fruit=fac->createFruit("apple");
	fruit->showName();
	delete fruit;
	fruit=fac->createFruit("banana");
	fruit->showName();
	delete fruit;
	fruit=fac->createFruit("pear");
	fruit->showName();
	delete fruit;
	delete fac;
}
int main(){
	test();
}
