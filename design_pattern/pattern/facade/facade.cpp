#include<iostream>

using namespace std;

/*
外观模式:为系统的一组接口提供一个一致的高层接口，使得客户不必与每个子功能打交道
(类比股票和基金)
ktv 模式  电视机打开  灯关闭 音响打开 麦克风打开 dvd打开
*/
class Tv{
public:
	void on(){
	cout<<"Tv on\n";
	}
	void off(){
	cout<<"Tv off\n";
	}
};
class Light{
public:
	void on(){
	cout<<"Light on\n";
	}
	void off(){
	cout<<"Light off\n";
	}
};
class Audio{
public:
	void on(){
	cout<<"Audio on\n";
	}
	void off(){
	cout<<"Audio off\n";
	}
};
class MicroPhone{
public:
	void on(){
	cout<<"MicroPhone on\n";
	}
	void off(){
	cout<<"MicroPhone off\n";
	}
};
class Dvd{
public:
	void on(){
	cout<<"Dvd on\n";
	}
	void off(){
	cout<<"Dvd off\n";
	}
};
class KtvFacade{
public:
	KtvFacade(){
		pTv=new Tv;
		pLight=new Light;
		pAudio=new Audio;
		pMicro=new MicroPhone;
		pDvd=new Dvd;
	}
	~KtvFacade(){
		delete pTv;
		delete pLight;
		delete pAudio;
		delete pMicro;
		delete pDvd;
	}
	void on(){
		cout<<"ktv mode on\n";
		pTv->on();
		pLight->on();
		pAudio->on();
		pMicro->on();
		pDvd->on();
	}
	void off(){
		pTv->off();
		pLight->off();
		pAudio->off();
		pMicro->off();
		pDvd->off();
		cout<<"ktv mode off\n";
	}
private:
Tv* pTv;
Light* pLight;
Audio*pAudio;
MicroPhone*pMicro;
Dvd* pDvd;
};
void test(){
	KtvFacade* k=new KtvFacade;
	k->on();
	k->off();
}
int main(){
	test();
}
