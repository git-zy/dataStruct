#include<iostream>
#include<list>

using namespace std;

/*
观察者模式又叫 发布-订阅模式(publish/subscribe)
定义了一个一对多的依赖关系，让多个观察者对象同时监听某一个主题对象
这个主题对象在发生变化的时候会通知所有观察者对象，使他们能够主动更新自己
(类比如下场景 5个英雄打Boss  Boss死后会通知每个活着的英雄)
*/

class AbstractHero{
public:
	virtual void UpDate()=0;
};

class HeroA:public AbstractHero{
public:
	HeroA(){
		cout<<"HeroA 正在打Boss...\n";
	}
	virtual void UpDate(){
		cout<<"HeroA 停止攻击，待机状态...\n";
	}
};
class HeroB:public AbstractHero{
public:
	HeroB(){
		cout<<"HeroB 正在打Boss...\n";
	}
	virtual void UpDate(){
		cout<<"HeroB 停止攻击，待机状态...\n";
	}
};
class HeroC:public AbstractHero{
public:
	HeroC(){
		cout<<"HeroC 正在打Boss...\n";
	}
	virtual void UpDate(){
		cout<<"HeroC 停止攻击，待机状态...\n";
	}
};
//观察者目标抽象
class AbstractBoss{
public:
	virtual void addHero(AbstractHero*hero)=0;
	virtual void deleteHero(AbstractHero*hero)=0;
	virtual void notify()=0;
};
class BossA:public AbstractBoss{
public:
	virtual void addHero(AbstractHero* hero){
		pHeroList.push_back(hero);
	}
	virtual void deleteHero(AbstractHero *hero){
		pHeroList.remove(hero);
	}
	virtual void notify (){
		for(auto it=pHeroList.begin();it!=pHeroList.end();it++){
			(*it)->UpDate();
		}
	}
private :
list<AbstractHero*> pHeroList;
};
void test(){
	//创建观察者
	AbstractHero* heroA=new HeroA;
	AbstractHero* heroB=new HeroB;
	AbstractHero* heroC=new HeroC;
	//创建观察目标
	AbstractBoss* bossA=new BossA;
	bossA->addHero(heroA);
	bossA->addHero(heroB);
	cout<<"HeroC 不幸阵亡...\n";
	bossA->deleteHero(heroC);
	cout<<"Boss 死亡  通知其他英雄...\n";
	bossA->notify();
}
int main(){
	test();
}
