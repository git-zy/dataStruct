#include<iostream>
#include<string>
using namespace std;
/*
代理模式:为其他对象提供一种代理以控制对这个对象的访问
一个抽象类中提供代理和实体都有的方法
代理和实体分别继承抽象类实现抽象类中提供的方法
代理类中含有一个实体类的变量，代理类的方法的实现调用的是
实体类的具体方法实现
*/
class AbstractSub{
public:
	virtual void run()=0;
};
class Real:public AbstractSub{
public:
	virtual void run(){
		cout<<"system start\n";
	}
};
//真实用户必须通过权限验证才可以访问系统,提供代理来控制访问权限
class Proxy:public AbstractSub{
public:
	Proxy(string name ,string password):m_name(name),m_password(password){
		pR=new Real;
	}
	bool check(){
		if(m_password=="root"&&m_name=="admin"){
		cout<<"username and password correct\n";
		return true;
		}
		return false;
	}
	virtual void run(){
		if(check()){
			pR->run();
		}
		else
		cout<<"username or password wrong\n";
	}
	~Proxy(){
		if(pR){
		delete pR;
		pR=NULL;
		}
	}
private:
string m_name ;
string m_password;
Real* pR;
};
void test(){
	Proxy *p=new Proxy("admin","root");
	p->run();
}
int main(){
	test();
}
