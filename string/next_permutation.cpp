#include<iostream>
#include<algorithm>


using namespace std;
/*
	后找：字符串中最后一个升序的位置is s[k]>s[k+1](k>i) s[i]<s[i+1]
	查找(小大)s[i+1...N-1]中比Ai大的最小值Sj
	交换：Si，Sj
	翻转：s[i+1...N-1]
		交换操作后，s[i+1...N-1]一定是降序的
*/
void Reverse(int *from,int*to){
	int t;
	while(from<to){
		t=*from;
		*from=*to;
		*to=t;
		from++;
		to--;
	}
}
bool next_permutation(int *a,int size){
	int i=size-2;//从最后的两个值开始比较
	//后找
	while((i>=0)&&(a[i]>=a[i+1]))	//寻找第一个升序的位置
	i--;
	if(i<0)  return false;
	int j=size-1;
	//小大
	while(a[j]<=a[i])//寻找第一个比i大一点的数字
	j--;
	//交换
	swap(a[i],a[j]);
	//翻转
	Reverse(a+i+1,a+size-1);
	return true;
}
void print(int*a,int size){
	for(int i=0;i<size;i++)
	cout<<a[i];
	cout<<endl;
}
int main(){
	int a[]={1,2,2,3};
	int size=sizeof(a)/sizeof(int);
	print(a,size);
	while(next_permutation(a,size))
	print(a,size);
	
}
