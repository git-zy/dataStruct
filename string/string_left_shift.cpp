#include<iostream>
#include<cstring>
#include<string>
using namespace std;
/*
	abcdef  左移两位  cdefab
	(x'y')'=yx  相当于线代的转置
	x=ab x'=ba  y=cdef y'=fedc  假如循环左移2位
	(x'y')'=(bafedc)'=cdefab;	
*/
void  reverse_string(char*s, int begin,int end){
	while(begin<end){
		char temp=s[begin];
		s[begin++]=s[end];
		s[end--]=temp;
	}
}
//m 左移位数  n字符串长度  左移m次  等于右移 n-m次
void string_left_shift(char* str,int m,int n){
	m%=n;//保证安全
	reverse_string(str,0,m-1);// 翻转3次即可完成
	reverse_string(str,m,n-1);
	reverse_string(str,0,n-1);
}

int main(){
	char str[]="abcde";
	char tmp[]="cdeab";
	string_left_shift(str,10,strlen(str));
	cout<<str<<endl;
}
