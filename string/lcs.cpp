#include<iostream>
#include <vector>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
/*
	最长公共子序列 不要求字母连续
	两个序列X和Y的公共子序列中最长的子序列定义为最长公共子序列
	13455 245576 最长为455  acdfg adfc  最长为adf
	lcs(Xm,Yn)=0  m=0或者n=0	
						=lcs(Xm-1,Yn-1)+Xm   当Xm=Yn
						=max(lcs(Xm-1,Yn),lcs(Xm,Yn-1))    Xm!=Yn
	
	str1[i]==str2[j] chess[i][j]=chess[i-1][j-1]+1
	str1[i]!=str2[j] chess[i][j]=max(chess[i-1][j],chess[i][j-1])
*/
int lcs(const char*str1,const char*str2,string&str){
	int len1=strlen(str1);
	int len2=strlen(str2);
	const char*s1=str1-1;//从1开始数，减1后str1指向0的前一个位置，循环直接从1开始str1[0]是原来str1[0]
	const char*s2=str2-1;
	vector<vector<int> >chess(len1+1, vector<int>(len2+1));//0行和0列为0
	int i,j;
	for(i=0;i<=len1;++i)
	chess[i][0]=0;//第0列
	for(j=0;i<=len2;++i)
	chess[0][j]=0;//第0行
	for(i=1;i<=len1;++i){
		for(j=1;j<=len2;++j){
			if(str1[i]==str2[j])
			chess[i][j]=chess[i-1][j-1]+1;
			else
			chess[i][j]=max(chess[i-1][j],chess[i][j-1]);
		}
	}
	i=len1;
	j=len2; //上面找位置，下面找字符串
	while((i!=0)&&(j!=0)){
		if(str1[i]==str2[j]){
		str.push_back(str1[i]);
		i--;
		j--;}
		else{
			if(chess[i-1][j]>chess[i][j-1])
			i--;//谁大谁减小
			else
			j--;
		}
		
	}
	reverse(str.begin(),str.end());//翻转
}
	

int main(){
	const char*str1="tcggatcgactt";
	const char*str2="agcctacgta";
	string str;
	lcs(str1,str2,str);
	cout<<str<<endl;
}
