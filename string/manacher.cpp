#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;

void manacher(char*s,int *p){
	int size=strlen(s);
	p[0]=1;
	int id=0;
	int mx=1;
	for(int i=1;i<size;i++)	{
		if(mx>i)	
		p[i]=min(p[2*id-i],mx-i);
		else
		p[i]=1;
		for(;s[i+p[i]]==s[i-p[i]];p[i]++)
			;
		if(mx<i+p[i]){
			mx=i+p[i];
			id=i;
		}
	}
}
int main(int argc,char**argv){
	char s[]="$#a#b#a#";
	int p[20]={0};
	manacher(s,p);
	for(int i=0;i<20;i++)
	cout<<i<<" "<<p[i]<<endl;
}
