#include<iostream>
#include<cstring>

using namespace std;
/*
	当模式串的最后一个字符与文本串的最后一个字符失配时只需要计算模式串的真前缀和真后缀的匹配程度
	就可以使模式串在移到的时候尽可能的多
*/
//计算next数组
void getNext(char*p,int*a){
	int len=(int)strlen(p);
	next[0]=-1;
	int k=-1;
	int j=0;
	while(j<len-1){
		//此刻，k即是next[j-1]，且p[k]表示前缀,p[j]表示后缀，
		//注：k==-1表示未找到k前缀与K后缀相等,首次分析可先忽略
		if(k==-1||p[j]==p[k]){
			++j;
			++k;
			next[j]=k; //对应于next[j+1]=next[j]+1
		}
		else
		k=next[k];//p[j]与p[k]失配,则继续递归计算前缀p[next[k]]
	}
}

