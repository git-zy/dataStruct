#include <iostream>
#include<string>
/*
kmp  字符串查找
给定文本串 text 和模式串pattern，从文本串中找到模式串第一次出现的位置
*/
using namespace std;
int force_search(const string& s,const string& p){
	if(s.empty()||p.empty())
	return 0;
	int i=0,j=0;
	int len=p.size();
	int last=s.size()-len;//文本串减去模式串的长度
	while((i<=last)&&j<len){
		if(s[i+j]==p[j])//若匹配，模式串的位置后移
		j++;
		else{//不匹配则文本串从下一个字符继续匹配，模式串从0开始
			i++;
			j=0;
		}
	}
	if(j>=len)//模式串走完了，说明匹配成功
	return i;
	return -1;
} 
int main(){
	string text="abcdefhello";
	string pattern ("fhello");
	int i=force_search(text,pattern);
	cout<<i<<endl;
}
