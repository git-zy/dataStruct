#include<iostream>
#include<cstring>
#include<cctype>
using namespace std;
//把字符串中的空格替换为%20
//计算出所需容纳空格的空间从尾后进行移动元素
void replace_blank(char *str,int length){
	if(!str||length<=0)
	return;
	int orign_len=0;
	int blank=0;
	int i=0;
	while(str[i]){
		orign_len++;
		if(isspace(str[i]))
		++blank;
		++i;
	}
	auto new_len=orign_len+blank*2;
	if(new_len>length)
	return;
	int index_orign=orign_len;
	int index_new=new_len;
	while(index_orign>=0&&index_new>index_orign){
		if(isspace(str[index_orign])){
			str[index_new--]='0';
			str[index_new--]='2';
			str[index_new--]='%';
		}
	else
	str[index_new--]=str[index_orign];
	--index_orign;
	}
}
int main(){
	
	char s[50]={0};
	strcpy(s,"we are young");
	replace_blank(s,50);
	cout<<s<<endl;
	return 0;
}
