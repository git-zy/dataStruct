#include<iostream>


using namespace std;
//给定字符串s[0...n-1] 求s的全排列
void print(const int*a,int size){
	for(int i=0;i<size;i++)
	cout<<a[i];
	cout<<endl;
}
bool isDuplicate(const int *a,int n,int t){
	while(n<t){
		if(a[n]==a[t])
		return false;
		n++;
	}
	return true;
}
// size 长度  n从哪一位开始做全排列
void permutation(int*a ,int size,int n){
	if(n==size-1){
		print(a,size);
		return;
	}
	for(int i=n;i<size;i++){
		if(!isDuplicate(a,n,i))//a[i]是否与a[n,i)重复
		continue;
		swap(a[i],a[n]);
		permutation(a,size,n+1);
		swap(a[i],a[n]);
	}
}
int main(){
	int a[]={1,2,2,3};
	permutation(a,sizeof(a)/sizeof(int),0);
	
}
