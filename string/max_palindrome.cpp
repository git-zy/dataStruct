#include<iostream>
#include<string>

using namespace std;

string max_palindrome(string s){
	if(s.size()<=1)
	return s.substr(0,1);
	int max=0;
	int i,j,cur=0,sub=0;
	int len=s.size();
	for(i=0;i<len;i++){
		for(j=0;i-j>=0&&i+j<len;j++){//奇数
			if(s[i-j]!=s[i+j])
			break;
			cur=2*j+1;
		}
		if(cur>max){
			max=cur;
			sub=i-j+1;
		}
		for(j=0;i-j>=0&&i+j+1<len;j++){
			if(s[i-j]!=s[i+j+1])
				break;
			cur=2*j+2;}
		if(cur>max){
			max=cur;
			sub=i-j+1;
		}
	}
	//return max;
	return s.substr(sub,max);
}

int main(int argc,char**argv){
	if(argc!=2)
	cerr<<"need a command line para\n";
	string s(argv[1]);
	cout<<max_palindrome(s)<<endl;
}
